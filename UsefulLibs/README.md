# Task 8 (Useful libraries)

After lecture, review your app. Find libraries that are applicable for your app and can simplify it. Integrate those libraries. Discuss your choices with mentor. Mentor evaluates your work on his own choice.

## Evaluation criteria
2. Apply Airbnb best practices to your project: clean up the code of your application: follow the required naming convention, make sure code has proper alignment and there are no redundant spaces, order all your methods in a right way.
3. Add storybook to your project. Use flow instead of Prop Types.
4. Use styled components / jss.
5. Use reselect/immutable.