/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
import * as React from 'react';
import { Utils } from './utils';

describe('Utils', () => {
  const movies = [
    {
      release_date: new Date('2017-08-24'),
      vote_average: 6,
      genres: ['genre 1', 'genre 2'],
      title: 'movie 1',
    },
    {
      release_date: new Date('2018-03-05'),
      vote_average: 6,
      genres: ['genre 5', 'genre 6'],
      title: 'movie 5',
    },
    {
      release_date: new Date('2016-03-12'),
      vote_average: 4,
      genres: ['genre 3', 'genre 4'],
      title: 'movie 2',
    },
    {
      release_date: new Date('2020-08-15'),
      vote_average: 5,
      genres: ['genre 4', 'genre 1'],
      title: 'movie 3',
    },
    {
      release_date: new Date('2020-06-01'),
      vote_average: 8,
      genres: ['genre 1', 'genre 5'],
      title: 'movie 4',
    },
  ];

  const expectedSortByReleaseDate = [
    {
      release_date: new Date('2020-08-15'),
      vote_average: 5,
      genres: ['genre 4', 'genre 1'],
      title: 'movie 3',
    },
    {
      release_date: new Date('2020-06-01'),
      vote_average: 8,
      genres: ['genre 1', 'genre 5'],
      title: 'movie 4',
    },
    {
      release_date: new Date('2018-03-05'),
      vote_average: 6,
      genres: ['genre 5', 'genre 6'],
      title: 'movie 5',
    },
    {
      release_date: new Date('2017-08-24'),
      vote_average: 6,
      genres: ['genre 1', 'genre 2'],
      title: 'movie 1',
    },
    {
      release_date: new Date('2016-03-12'),
      vote_average: 4,
      genres: ['genre 3', 'genre 4'],
      title: 'movie 2',
    },
  ];

  const expectedSortByRating = [
    {
      release_date: new Date('2020-06-01'),
      vote_average: 8,
      genres: ['genre 1', 'genre 5'],
      title: 'movie 4',
    },
    {
      release_date: new Date('2017-08-24'),
      vote_average: 6,
      genres: ['genre 1', 'genre 2'],
      title: 'movie 1',
    },
    {
      release_date: new Date('2018-03-05'),
      vote_average: 6,
      genres: ['genre 5', 'genre 6'],
      title: 'movie 5',
    },
    {
      release_date: new Date('2020-08-15'),
      vote_average: 5,
      genres: ['genre 4', 'genre 1'],
      title: 'movie 3',
    },
    {
      release_date: new Date('2016-03-12'),
      vote_average: 4,
      genres: ['genre 3', 'genre 4'],
      title: 'movie 2',
    },
  ];

  const expectedSearchByTitle = [
    {
      release_date: new Date('2020-08-15'),
      vote_average: 5,
      genres: ['genre 4', 'genre 1'],
      title: 'movie 3',
    },
  ];

  const expectedSearchByGenre = [
    {
      release_date: new Date('2016-03-12'),
      vote_average: 4,
      genres: ['genre 3', 'genre 4'],
      title: 'movie 2',
    },
    {
      release_date: new Date('2020-08-15'),
      vote_average: 5,
      genres: ['genre 4', 'genre 1'],
      title: 'movie 3',
    },
  ];

  it('sorts by release date as expected', () => {
    const actualStortByReleaseDate = Utils.sortByReleaseDate(movies);

    expect(actualStortByReleaseDate).toEqual(expectedSortByReleaseDate);
  });

  it('sorts by rating as expected', () => {
    const actualStortByRating = Utils.sortByRating(movies);

    expect(actualStortByRating).toEqual(expectedSortByRating);
  });

  it('search by title as expected', () => {
    const title = 'movie 3';

    const actualSearchResult = Utils.searchByTitle(title, movies);

    expect(actualSearchResult).toEqual(expectedSearchByTitle);
  });

  it('search by genres as expected', () => {
    const genre = 'genre 4';

    const actualSearchResult = Utils.searchByGenre(genre, movies);

    expect(actualSearchResult).toEqual(expectedSearchByGenre);
  });
});
