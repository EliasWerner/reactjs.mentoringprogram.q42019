// @flow

import { List } from 'immutable';
import * as moviesActionTypes from './moviesActionTypes';
import { initialMoviesState } from './moviesState';
import type { MovieType, MoviesStateType } from './moviesState';

export const moviesReducer = (
  state: MoviesStateType = initialMoviesState,
  action: any
) => {
  switch (action.type) {
    case moviesActionTypes.SET_CURRENT_MOVIE: {
      return { ...state, currentMovie: action.movie };
    }
    case moviesActionTypes.SET_MOVIES_WITH_THE_SAME_GENRE: {
      return { ...state, moviesWithTheSameGenre: List(action.movies) };
    }
    case moviesActionTypes.SET_MOVIES: {
      return { ...state, movies: List(action.movies) };
    }
    case moviesActionTypes.SET_MOVIES_TO_SHOW: {
      return { ...state, moviesToShow: List(action.movies) };
    }
    case moviesActionTypes.SET_IS_LOADING: {
      return { ...state, isFetching: action.isLoading };
    }
    case moviesActionTypes.SET_SEARCH_FILTER: {
      return {
        ...state,
        searchFilter: action.searchFilter,
      };
    }
    case moviesActionTypes.SET_SORTING_FILTER: {
      return {
        ...state,
        sortingFilter: action.sortingFilter,
      };
    }
    case moviesActionTypes.RESET_STORE: {
      return {
        ...action.store,
      };
    }
    case moviesActionTypes.SET_PAGE_SEPARATOR_TEXT: {
      return {
        ...state,
        pageSeparatorText: action.text,
      };
    }
    default:
      return state;
  }
};
export default moviesReducer;
