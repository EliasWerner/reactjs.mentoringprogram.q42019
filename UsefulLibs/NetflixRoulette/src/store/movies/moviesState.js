// @flow

import { List } from 'immutable';
import { SearchFilters, SortingFilters } from './moviesActions';

export type MovieType = {
  id: number,
  title: string,
  tagline: string,
  vote_average: number,
  vote_count: number,
  release_date: string,
  poster_path: string,
  overview: string,
  budget: number,
  revenue: number,
  genres: Array<string>,
  runtime: number,
};

export type MoviesStateType = {
  movies: List<MovieType>,
  moviesToShow: List<MovieType>,
  moviesWithTheSameGenre: List<MovieType>,
  currentMovie: MovieType | null,
  sortingFilter: string,
  searchFilter: string,
  pageSeparatorText: string,
  isFetching: boolean,
};

export const initialMoviesState: MoviesStateType = {
  movies: List(),
  moviesToShow: List(),
  moviesWithTheSameGenre: List(),
  currentMovie: null,
  sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
  searchFilter: SearchFilters.SEARCH_BY_TITLE,
  pageSeparatorText: '',
  isFetching: false,
};
