/* eslint-disable no-undef */
import { moviesReducer } from './moviesReducer';
import * as actionTypes from './moviesActionTypes';
import { SearchFilters, SortingFilters } from './moviesActions';

describe('MoviesReducer', () => {
  const initialMoviesState = {
    movies: [],
    moviesToShow: [],
    currentMovie: null,
    sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
    searchFilter: SearchFilters.SEARCH_BY_TITLE,
    pageSeparatorText: '',
  };

  it('returns initial state', () => {
    const actualResult = moviesReducer(undefined, {});

    expect(actualResult).toEqual(initialMoviesState);
  });

  it('sets currentMovie to state as axpected', () => {
    const setCurrentMovieAction = {
      type: actionTypes.SET_CURRENT_MOVIE,
      movie: 'movie 1',
    };
    const actualResult = moviesReducer(
      initialMoviesState,
      setCurrentMovieAction
    ).currentMovie;

    expect(actualResult).toEqual(setCurrentMovieAction.movie);
  });

  it('sets movies to state as axpected', () => {
    const setMoviesAction = {
      type: actionTypes.SET_MOVIES,
      movies: ['movie 1', 'movie 2', 'movie 3'],
    };
    const actualResult = moviesReducer(initialMoviesState, setMoviesAction)
      .movies;

    expect(actualResult).toEqual(setMoviesAction.movies);
  });

  it('sets moviesToShow to state as axpected', () => {
    const setMoviesToShowAction = {
      type: actionTypes.SET_MOVIES_TO_SHOW,
      movies: ['movie 1', 'movie 2', 'movie 3'],
    };
    const actualResult = moviesReducer(
      initialMoviesState,
      setMoviesToShowAction
    ).moviesToShow;

    expect(actualResult).toEqual(setMoviesToShowAction.movies);
  });

  it('sets searchFilter to state as axpected', () => {
    const setSearchFilterAction = {
      type: actionTypes.SET_SEARCH_FILTER,
      searchFilter: SearchFilters.SEARCH_BY_GENRE,
    };
    const actualResult = moviesReducer(
      initialMoviesState,
      setSearchFilterAction
    ).searchFilter;

    expect(actualResult).toEqual(setSearchFilterAction.searchFilter);
  });

  it('sets sortingFilter to state as axpected', () => {
    const setSortingFilterAction = {
      type: actionTypes.SET_SORTING_FILTER,
      sortingFilter: SortingFilters.SORT_BY_RATING,
    };
    const actualResult = moviesReducer(
      initialMoviesState,
      setSortingFilterAction
    ).sortingFilter;

    expect(actualResult).toEqual(setSortingFilterAction.sortingFilter);
  });

  it('sets pageSeparatorText to state as axpected', () => {
    const setPageSeparatorTextAction = {
      type: actionTypes.SET_PAGE_SEPARATOR_TEXT,
      text: '1 movie found',
    };
    const actualResult = moviesReducer(
      initialMoviesState,
      setPageSeparatorTextAction
    ).pageSeparatorText;

    expect(actualResult).toEqual(setPageSeparatorTextAction.text);
  });

  it('restore state as axpected', () => {
    const setRestoreAction = {
      type: actionTypes.RESET_STORE,
      store: {
        movies: ['movie 1', 'movie 2'],
        moviesToShow: ['movie 1', 'movie 2'],
        currentMovie: null,
        sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
        searchFilter: SearchFilters.SEARCH_BY_TITLE,
      },
    };
    const actualResult = moviesReducer(initialMoviesState, setRestoreAction);

    expect(actualResult).toEqual(setRestoreAction.store);
  });
});
