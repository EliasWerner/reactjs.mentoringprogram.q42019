/* eslint-disable no-undef */

import configureMockStore from 'redux-mock-store';
import thunkMiddleware from 'redux-thunk';
import * as actionTypes from './moviesActionTypes';
import {
  setMovies,
  setMoviesToShow,
  setCurrentMovie,
  SearchFilters,
  setSearchFilter,
  SortingFilters,
  setSortingFilter,
  getMovies,
  restore,
  setStore,
  getMovieById,
  getMoviesWithTheSameGenre,
  searchMovies,
  sortMovies,
} from './moviesActions';
import { Utils } from '../../utils/utils';

jest.mock('../../utils/utils');
jest.mock('../../services/moviesService');

const initialMoviesState = {
  movies: [
    { id: 1, genres: ['genre 1, genre 2'] },
    { id: 2, genres: ['genre 1, genre 3'] },
    { id: 3, genres: ['genre 2, genre 3'] },
  ],
  moviesToShow: [],
  currentMovie: { id: 1, genres: ['genre 1, genre 2'] },
  sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
  searchFilter: SearchFilters.SEARCH_BY_TITLE,
  pageSeparatorText: '',
};

describe('MoviesActions', () => {
  let store;
  const createStore = configureMockStore([thunkMiddleware]);

  beforeEach(() => {
    store = createStore({ moviesState: initialMoviesState });
  });

  it('creates setStore action as expected', () => {
    const s = {
      movies: ['movie 1', 'movie 2'],
      moviesToShow: ['movie 1', 'movie 2'],
      currentMovie: null,
      sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
      searchFilter: SearchFilters.SEARCH_BY_TITLE,
    };

    const actualSetStoreAction = setStore(s);

    expect(actualSetStoreAction.store).toEqual(s);
    expect(actualSetStoreAction.type).toEqual(actionTypes.RESET_STORE);
  });

  it('creates setMovies action as expected', () => {
    const movies = ['movie 1', 'movie 2'];

    const actualSetMoviesAction = setMovies(movies);

    expect(actualSetMoviesAction.movies).toEqual(movies);
    expect(actualSetMoviesAction.type).toEqual(actionTypes.SET_MOVIES);
  });

  it('creates setMoviesToShow  action as expected', () => {
    const movies = ['movie 1', 'movie 2'];

    const actualSetMoviesToShow = setMoviesToShow(movies);

    expect(actualSetMoviesToShow.movies).toEqual(movies);
    expect(actualSetMoviesToShow.type).toEqual(actionTypes.SET_MOVIES_TO_SHOW);
  });

  it('creates setCurrentMovie action as expected', () => {
    const movie = 'movie 1';

    const actualSetCurrentMovie = setCurrentMovie(movie);

    expect(actualSetCurrentMovie.movie).toEqual(movie);
    expect(actualSetCurrentMovie.type).toEqual(actionTypes.SET_CURRENT_MOVIE);
  });

  it('creates setSearchFilter  action as expected', () => {
    const searchFiler = SearchFilters.SEARCH_BY_GENRE;

    const actualSetSearchFilter = setSearchFilter(searchFiler);

    expect(actualSetSearchFilter.searchFilter).toEqual(searchFiler);
    expect(actualSetSearchFilter.type).toEqual(actionTypes.SET_SEARCH_FILTER);
  });

  it('creates setCurrentMovie action as expected', () => {
    const sortingFiler = SortingFilters.SORT_BY_RATING;

    const actualSetSortingFilter = setSortingFilter(sortingFiler);

    expect(actualSetSortingFilter.sortingFilter).toEqual(sortingFiler);
    expect(actualSetSortingFilter.type).toEqual(actionTypes.SET_SORTING_FILTER);
  });

  it('getMovies action dispatch actions as expected', () => {
    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES,
      },
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];
    store.dispatch(getMovies()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('restore action dispatch actions as expected', () => {
    const expectedActions = [
      {
        store: {
          movies: [],
          moviesToShow: [],
          currentMovie: null,
          sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
          searchFilter: SearchFilters.SEARCH_BY_TITLE,
          pageSeparatorText: '',
        },
        type: actionTypes.RESET_STORE,
      },
    ];
    store.dispatch(restore()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('getMovieById action dispatch actions as expected', () => {
    const expectedActions = [
      {
        movie: null,
        type: actionTypes.SET_CURRENT_MOVIE,
      },
    ];

    store.dispatch(getMovieById(1)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('getMoviesWithTheSameGenre action dispatch actions as expected', () => {
    const moviesWithTheSameGenre = [
      { id: 2, genres: ['genre 1, genre 3'] },
      { id: 3, genres: ['genre 2, genre 3'] },
    ];
    Utils.searchByGenre = jest.fn(() => moviesWithTheSameGenre);
    const expectedActions = [
      {
        movies: moviesWithTheSameGenre,
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(getMoviesWithTheSameGenre()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('searchMovies action dispatches Utils.searchByTitle function when searchFilter = SEARCH_BY_TITLE', () => {
    Utils.searchByTitle = jest.fn(() => ['movie 1']);
    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
      {
        text: '1 movie found',
        type: actionTypes.SET_PAGE_SEPARATOR_TEXT,
      },
    ];

    store.dispatch(searchMovies('some string')).then(() => {
      expect(Utils.searchByTitle).toHaveBeenCalled();
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('searchMovies action dispatches Utils.searchByGenre function when searchFilter = SEARCH_BY_GENRE', () => {
    store = createStore({
      moviesState: {
        ...initialMoviesState,
        searchFilter: SearchFilters.SEARCH_BY_GENRE,
      },
    });

    Utils.searchByGenre = jest.fn();
    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(searchMovies('some string')).then(() => {
      expect(Utils.searchByGenre).toHaveBeenCalled();
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('searchMovies action dispatches setsMoviesToShow action with empty array by default', () => {
    store = createStore({
      moviesState: {
        ...initialMoviesState,
        searchFilter: 'Some filter',
      },
    });

    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(searchMovies('some string')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('sortMovies action dispatches Utils.sortByReleaseDate function when sortingFilter = SORT_BY_RELEASE_DATE', () => {
    Utils.sortByReleaseDate = jest.fn();
    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(sortMovies()).then(() => {
      expect(Utils.sortByReleaseDate).toHaveBeenCalled();
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('sortMovies action dispatches Utils.sortByRating function when sortingFilter = SORT_BY_RATING', () => {
    store = createStore({
      moviesState: {
        ...initialMoviesState,
        sortingFilter: SortingFilters.SORT_BY_RATING,
      },
    });

    Utils.sortByRating = jest.fn();
    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(sortMovies()).then(() => {
      expect(Utils.sortByRating).toHaveBeenCalled();
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('sortMovies action dispatches setsMoviesToShow action with empty array by default', () => {
    store = createStore({
      moviesState: {
        ...initialMoviesState,
        sortingFilter: 'Some filter',
      },
    });

    const expectedActions = [
      {
        movies: [],
        type: actionTypes.SET_MOVIES_TO_SHOW,
      },
    ];

    store.dispatch(sortMovies()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
