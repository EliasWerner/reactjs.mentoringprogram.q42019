// @flow

/* eslint-disable operator-linebreak */
import { List } from 'immutable';
import { MoviesService } from '../../services/moviesService';
import * as ActionTypes from './moviesActionTypes';
import { Utils } from '../../utils/utils';
import type { MovieType, MoviesStateType } from './moviesState';

export const SearchFilters = {
  SEARCH_BY_TITLE: 'SEARCH_BY_TITLE',
  SEARCH_BY_GENRE: 'SEARCH_BY_GENRE',
};

export const SortingFilters = {
  SORT_BY_RELEASE_DATE: 'SORT_BY_RELEASE_DATE',
  SORT_BY_RATING: 'SORT_BY_RATING',
};

export const setIsLoading = (isLoading: boolean) => ({
  isLoading,
  type: ActionTypes.SET_IS_LOADING,
});

/* SET_PAGE_SEPARATOR_TEXT */

export const setPageSeparatorText = (text: string) => ({
  text,
  type: ActionTypes.SET_PAGE_SEPARATOR_TEXT,
});

/* SET_MOVIES_WITH_THE_SAME_GENRE */

export const setMoviesWithTheSameGenre = (movies: Array<MovieType>) => ({
  movies,
  type: ActionTypes.SET_MOVIES_WITH_THE_SAME_GENRE,
});

/* RESET_STORE */

export const setStore = (store: MoviesStateType) => ({
  store,
  type: ActionTypes.RESET_STORE,
});

/* SET_MOVIES */

export const setMovies = (movies: Array<MovieType>) => ({
  movies,
  type: ActionTypes.SET_MOVIES,
});

export const setMoviesToShow = (movies: Array<MovieType>) => ({
  movies,
  type: ActionTypes.SET_MOVIES_TO_SHOW,
});

/* SET_CURRENT_MOVIE */

export const setCurrentMovie = (movie: MovieType | null) => ({
  movie,
  type: ActionTypes.SET_CURRENT_MOVIE,
});

/* SET_SEARCH_FILTER */
export const setSearchFilter = (searchFilter: string) => ({
  searchFilter,
  type: ActionTypes.SET_SEARCH_FILTER,
});

/* SET_SORTING_FILTER */
export const setSortingFilter = (sortingFilter: string) => ({
  sortingFilter,
  type: ActionTypes.SET_SORTING_FILTER,
});

export const getMovies = () => async (dispatch: Function) => {
  try {
    dispatch(setIsLoading(true));
    const service = new MoviesService();
    const movies = await service.getMovies();

    const sortedMovies = Utils.sortByReleaseDate(movies);

    dispatch(setMovies(sortedMovies));
  } catch (error) {
    throw new Error(`Could not get movies: ${error.message}`);
  }
};

export const restore = () => async (dispatch: Function, getState: Function) => {
  const { movies } = getState().moviesState.toArray();

  const store: MoviesStateType = {
    movies,
    moviesToShow: List(),
    moviesWithTheSameGenre: List(),
    currentMovie: null,
    sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
    searchFilter: SearchFilters.SEARCH_BY_TITLE,
    pageSeparatorText: '',
    isFetching: false,
  };

  dispatch(setStore(store));
};

export const getMovieById = (movieId: number) => async (dispatch: Function) => {
  try {
    dispatch(setIsLoading(true));
    const service = new MoviesService();
    const movie = await service.getMovieById(movieId);

    dispatch(setCurrentMovie(movie));
    dispatch(setIsLoading(false));
  } catch (error) {
    throw new Error(`Could not get movie by id: ${error.message}`);
  }
};

export const getMoviesWithTheSameGenre = () => async (
  dispatch: Function,
  getState: Function
) => {
  const movie = getState().moviesState.currentMovie;
  const movies = getState().moviesState.movies.toArray();

  const moviesWithTheSameGenre = Utils.searchByGenre(movie.genres[0], movies);
  const filteredMovies = moviesWithTheSameGenre.filter(
    (m) => m.id !== movie.id
  );

  dispatch(setMoviesWithTheSameGenre(filteredMovies));
};

export const searchMovies = (filterString: string) => async (
  dispatch: Function,
  getState: Function
) => {
  const { searchFilter } = getState().moviesState;
  const movies = getState().moviesState.movies.toArray();

  let filteredMovies = [];
  switch (searchFilter) {
    case SearchFilters.SEARCH_BY_TITLE:
      filteredMovies = Utils.searchByTitle(filterString, movies);
      break;
    case SearchFilters.SEARCH_BY_GENRE:
      filteredMovies = Utils.searchByGenre(filterString, movies);
      break;
    default:
      break;
  }

  const separatorText = filteredMovies.length
    ? `${filteredMovies.length} movie found`
    : '';

  dispatch(setMoviesToShow(filteredMovies));
  dispatch(setPageSeparatorText(separatorText));
};

export const sortMovies = () => async (
  dispatch: Function,
  getState: Function
) => {
  const { sortingFilter } = getState().moviesState;
  const movies = getState().moviesState.moviesToShow.toArray();

  let sortedMovies = [];
  switch (sortingFilter) {
    case SortingFilters.SORT_BY_RELEASE_DATE: {
      sortedMovies = Utils.sortByReleaseDate(movies);
      break;
    }
    case SortingFilters.SORT_BY_RATING: {
      sortedMovies = Utils.sortByRating(movies);
      break;
    }
    default:
      break;
  }

  dispatch(setMoviesToShow(sortedMovies));
};

/*    Actions for SSR     */

export const fetchMovieById = (id: number | string) => async (
  dispatch: Function
) => {
  try {
    dispatch(setIsLoading(true));
    const service = new MoviesService();
    const movies = await service.getMovies();

    const movie = await service.getMovieById(id);

    dispatch(setCurrentMovie(movie));
    dispatch(setMovies(movies));

    const moviesWithTheSameGenre = Utils.searchByGenre(movie.genres[0], movies);
    const filteredMovies = moviesWithTheSameGenre.filter(
      (m) => m.id !== movie.id
    );

    setMoviesWithTheSameGenre(filteredMovies);
    dispatch(setIsLoading(false));
  } catch (e) {
    dispatch(setCurrentMovie(null));
    dispatch(setIsLoading(false));
  }
};

export const fetchMovies = () => async (dispatch: Function) => {
  try {
    dispatch(setIsLoading(true));
    const service = new MoviesService();
    const movies = await service.getMovies();

    dispatch(setMovies(movies));
  } catch (e) {
    dispatch(setMovies([]));
    throw new Error(`Could not get movies: ${e.message}`);
  }
};

export const fetchMoviesAndPerformSearch = (
  searchFilter: string,
  searchQuery: string
) => async (dispatch: Function) => {
  try {
    dispatch(setIsLoading(true));

    const filter =
      searchFilter === 'genre'
        ? SearchFilters.SEARCH_BY_GENRE
        : SearchFilters.SEARCH_BY_TITLE;
    dispatch(setSearchFilter(filter));

    const service = new MoviesService();
    const movies = await service.getMovies();

    const moviesToShow =
      filter === SearchFilters.SEARCH_BY_GENRE
        ? Utils.searchByGenre(searchQuery, movies)
        : Utils.searchByTitle(searchQuery, movies);

    dispatch(setMovies(movies));
    dispatch(setMoviesToShow(moviesToShow));
  } catch (e) {
    dispatch(setMovies([]));
    throw new Error(`Could not get movies: ${e.message}`);
  }
};
