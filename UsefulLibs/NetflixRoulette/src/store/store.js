// @flow

import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { moviesReducer } from './movies/moviesReducer';

const rootReducer = combineReducers({ moviesState: moviesReducer });

export function configureStore() {
  const store = createStore(rootReducer, {}, applyMiddleware(thunkMiddleware));

  return store;
}
