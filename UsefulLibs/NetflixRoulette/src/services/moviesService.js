// @flow

import axios from 'axios';

export class MoviesService {
  apiEndpoint = 'https://reactjs-cdp.herokuapp.com/movies';

  getMovies = async () => {
    try {
      const res = await axios.get(this.apiEndpoint);

      return res.data.data;
    } catch (error) {
      throw new Error(`Error occured while web api:${error.message}`);
    }
  };

  getMovieById = async (id: number | string) => {
    try {
      const res = await axios.get(`${this.apiEndpoint}/${id}`);

      console.log(res.data);
      return res.data;
    } catch (error) {
      throw new Error(`Error occured while web api:${error.message}`);
    }
  };
}
