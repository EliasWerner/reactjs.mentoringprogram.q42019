import * as React from 'react';
import jss from 'jss';
import { createUseStyles } from 'react-jss';

const footerStyles = createUseStyles({
  footer: {
    textAlign: 'center',
    fontSize: '24px',
    color: '#f65261',
    paddingTop: '15px',
    paddingBottom: '15px',
    backgroundColor: '#424242',
  },
  netflix: {
    fontFamily: 'Franklin Gothic Heavy',
    letterSpacing: '0.01em',
  },
});

export const Footer = () => {
  const styles = footerStyles();

  return (
    <footer className={styles.footer}>
      <b className={styles.netflix}>netflix</b>roulette
    </footer>
  );
};
