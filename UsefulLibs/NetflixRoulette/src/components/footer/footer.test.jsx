/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import { Footer } from './footer.jsx';

describe('<Footer />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Footer />);
  });

  it('renders Footer element as expected', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
