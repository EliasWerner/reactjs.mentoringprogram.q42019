/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import App from './app.jsx';

describe('<App />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it('renders App element', () => {
    expect(wrapper.exists()).toBeTruthy();
  });
});
