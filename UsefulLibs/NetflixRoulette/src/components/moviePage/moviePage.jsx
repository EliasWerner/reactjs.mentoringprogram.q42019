// @flow

import * as React from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { MoviePageSeparator } from './moviePageSeparator/moviePageSeparator.jsx';
import { MovieInfo } from './movieInfo/movieInfo.jsx';
import { MoviesList } from '../moviesListPage/moviesList/moviesList.jsx';
import { Utils } from '../../utils/utils';
import { fetchMovieById } from '../../store/movies/moviesActions';
import type { MovieType } from '../../store/movies/moviesState';
import styles from './moviePage.css';

type MoviePageProps = {
  movie: MovieType,
  moviesToShow: Array<MovieType>,
  location: {
    state: {
      movieId: number,
    },
  },
  match: {
    params: {
      id: string,
    },
  },
  isLoading: boolean,
  getMovieById: Function,
  getMoviesWithTheSameGenre: Function,
};

export class MoviePage extends React.Component<MoviePageProps, {}> {
  static fetching({ dispatch, path }: { dispatch: Function, path: string }) {
    return [dispatch(fetchMovieById(path.substr(6)))];
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.getMovieById(id);
  }

  componentDidUpdate(prevProps: MoviePageProps) {
    const currentMovieId = this.props.match.params.id;
    const prevMovieId = prevProps.match.params.id;

    if (currentMovieId !== prevMovieId) {
      this.props.getMovieById(currentMovieId);
    }

    if (prevProps.isLoading && !this.props.isLoading) {
      this.props.getMoviesWithTheSameGenre();
    }
  }

  render() {
    return (
      <div>
        <div className={styles.pageHeader}>
          {!this.props.isLoading && this.props.movie ? (
            <MovieInfo movie={this.props.movie} />
          ) : (
            <Spinner animation="border" role="status" variant="light">
              <span className="sr-only">Loading...</span>
            </Spinner>
          )}
        </div>
        <div className={styles.pageSeparator}>
          {this.props.movie && !Utils.isEmptyObject(this.props.movie) && (
            <MoviePageSeparator genre={this.props.movie.genres[0]} />
          )}
        </div>
        {this.props.movie && !Utils.isEmptyObject(this.props.movie) && (
          <MoviesList movies={this.props.moviesToShow} />
        )}
      </div>
    );
  }
}
