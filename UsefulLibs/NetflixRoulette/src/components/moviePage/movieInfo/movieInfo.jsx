import * as React from 'react';
import { Link } from 'react-router-dom';
import { movieInfoStyles } from './movieInfoStyles';
import { Utils } from '../../../utils/utils';
import type { MovieType } from '../../../store/movies/moviesState';

type MovieInfoProps = {
  movie: MovieType,
};

export const MovieInfo = (props: MovieInfoProps) => {
  const { movie } = props;
  const styles = movieInfoStyles();
  return (
    <>
      {!Utils.isEmptyObject(props.movie) ? (
        <div className={styles.movieInfo}>
          <Link
            to={{
              pathname: '/',
              state: { shouldResetState: true },
            }}
            className={styles.homeLink}
          >
            &#x2315;
          </Link>{' '}
          <div className={styles.movieInfoRow}>
            <div className={styles.movieInfoColumn}>
              <img
                className={styles.moviePicture}
                src={movie.poster_path}
                alt={movie.title}
              />
            </div>
            <div className={styles.movieInfoColumn}>
              <div className={styles.movieTitle}>
                <div className={styles.title}>{movie.title}</div>
                <div className={styles.rating}>{movie.vote_average}</div>
              </div>
              <div className={styles.genres}>{movie.genres.join(', ')}</div>
              <div className={styles.releaseInfo}>
                <span className={styles.redText}>
                  {new Date(movie.release_date).getFullYear()}
                </span>{' '}
                year <span className={styles.redText}>{movie.runtime}</span> min
              </div>
              <div className={styles.overview}>{movie.overview}</div>
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.movieNotFound}>
          <h1>Movie is not found</h1>
        </div>
      )}
    </>
  );
};
