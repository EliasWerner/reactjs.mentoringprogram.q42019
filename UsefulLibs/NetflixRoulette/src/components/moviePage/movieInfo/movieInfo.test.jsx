/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import { MovieInfo } from './movieInfo.jsx';

describe('<MovieInfo />', () => {
  let wrapper;

  const movieInfoProps = {
    movie: {
      id: 1,
      title: 'Film Title',
      tagline: 'Film Tagline',
      vote_average: 5,
      vote_count: 100,
      release_date: '2019-09-09',
      poster_path:
        'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
      overview: 'Film overview',
      budget: 100000,
      revenue: 1000000,
      genres: ['Genre 1', 'Genre 2'],
      runtime: 100,
    },
  };

  beforeEach(() => {
    wrapper = shallow(<MovieInfo {...movieInfoProps} />);
  });

  it('renders MovieInfo element', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('renders movies Title as expected', () => {
    const actualMovieTitle = wrapper.find('.title').text();

    expect(actualMovieTitle).toEqual(movieInfoProps.movie.title);
  });
});
