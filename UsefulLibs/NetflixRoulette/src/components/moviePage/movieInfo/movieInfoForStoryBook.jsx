import * as React from 'react';
import { createUseStyles } from 'react-jss';
import type { MovieType } from '../../../store/movies/moviesState';

export const movieInfoStyles = createUseStyles({
  movieInfoTitle: {
    display: 'inline-block',
    fontSize: '36px',
  },
});

type MovieInfoForStoryBookProps = {
  movie: MovieType,
};

export const MovieTitle = (props: MovieInfoForStoryBookProps) => {
  const { movie } = props;
  const styles = movieInfoStyles();
  console.log(movie);

  return (
    <>
      {movie ? (
        <div>
          <div className={styles.movieInfoTitle}>{movie.title}</div>
        </div>
      ) : (
        <div>
          <h1>Movie is not found</h1>
        </div>
      )}
    </>
  );
};
