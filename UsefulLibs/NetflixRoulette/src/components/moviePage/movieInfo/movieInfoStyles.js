import { createUseStyles } from 'react-jss';

export const movieInfoStyles = createUseStyles({
  movieInfo: {
    fontFamily: 'Segoe UI Light',
    color: '#ffffff',
  },

  moviePicture: {
    height: '600px',
  },

  movieInfoRow: {
    display: 'inline-block',
  },

  movieInfoColumn: {
    display: 'inline-block',
    marginLeft: '50px',
    verticalAlign: 'top',
  },

  movieNotFound: {
    textAlign: 'center',
    paddingTop: '300px',
    paddingBottom: '300px',
    fontFamily: 'Segoe UI Light',
    color: '#ffffff',
  },

  movieTitle: {
    display: 'inline-block',
    marginBottom: '10px',
  },

  title: {
    display: 'inline-block',
    fontSize: '36px',
  },

  rating: {
    display: 'inline-block',
    fontSize: '36px',
    color: '#a1e66f',
    borderRadius: '100%',
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: '1px',
    paddingLeft: '10px',
    paddingRight: '10px',
    marginLeft: '10px',
  },

  genres: {
    fontSize: '22px',
    marginBottom: '10px',
  },

  releaseInfo: {
    fontSize: '16px',
    marginBottom: '10px',
  },

  redText: {
    fontSize: '26px',
    color: '#f65261',
  },

  overview: {
    width: '1200px',
    fontSize: '22px',
  },

  homeLink: {
    textAlign: 'left',
    fontSize: '45px',
    color: '#f65261',
    top: '0px',
    position: 'absolute',
    textDecoration: 'none',
    marginLeft: '95%',
    '&:hover': {
      color: '#f65261',
      textDecoration: 'none',
    },
  },
});
