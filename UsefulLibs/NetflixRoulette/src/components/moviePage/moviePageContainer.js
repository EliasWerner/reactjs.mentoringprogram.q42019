import { connect } from 'react-redux';
import { MoviePage } from './moviePage.jsx';
import {
  getMovieById,
  getMoviesWithTheSameGenre,
} from '../../store/movies/moviesActions';
import type { MoviesStateType } from '../../store/movies/moviesState';

export const mapStateToProps = (state: { moviesState: MoviesStateType }) => ({
  isLoading: state.moviesState.isFetching,
  movie: state.moviesState.currentMovie,
  moviesToShow: state.moviesState.moviesWithTheSameGenre.toArray(),
  movies: state.moviesState.movies.toArray(),
});

export const mapDispatchToProps = (dispatch: Function) => ({
  getMovieById: (id: number) => {
    dispatch(getMovieById(id));
  },
  getMoviesWithTheSameGenre: () => {
    dispatch(getMoviesWithTheSameGenre());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviePage);
