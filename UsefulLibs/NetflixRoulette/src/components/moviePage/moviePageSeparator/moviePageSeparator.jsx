import * as React from 'react';
import { createUseStyles } from 'react-jss';

const separatorStyles = createUseStyles({
  moviePageSeparator: {
    fontWeight: 'bold',
    marginLeft: '150px',
  },
});

type MoviePageSeparatorProps = {
  genre: string,
};

export const MoviePageSeparator = (props: MoviePageSeparatorProps) => {
  const styles = separatorStyles();

  return (
    <div className={styles.moviePageSeparator}>
      Movies by the {props.genre} genre
    </div>
  );
};
