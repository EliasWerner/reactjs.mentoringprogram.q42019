/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import { MoviePageSeparator } from './moviePageSeparator.jsx';

describe('<MoviePageSeparator />', () => {
  let wrapper;

  const moviePageSeparatorProps = {
    genre: 'Genre 1',
  };

  beforeEach(() => {
    wrapper = shallow(<MoviePageSeparator {...moviePageSeparatorProps} />);
  });

  it('renders MoviePageSeparator element as expected', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
