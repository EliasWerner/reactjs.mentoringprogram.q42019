import { createUseStyles } from 'react-jss';

export const movieListStyles = createUseStyles({
  moviesList: {
    backgroundColor: '#232323',
    paddingTop: '50px',
    paddingBottom: '50px',
  },

  moviesListContainer: {
    maxWidth: '90%',
  },

  movieColumn: {
    maxWidth: '30%',
    margin: '5px',
    marginRight: '2em',
  },

  emptyResult: {
    paddingTop: '150px',
    paddingBottom: '150px',
    textAlign: 'center',
    color: '#dedede',
    fontFamily: 'Segoe UI Light',
    fontSize: '36px',
  },
});
