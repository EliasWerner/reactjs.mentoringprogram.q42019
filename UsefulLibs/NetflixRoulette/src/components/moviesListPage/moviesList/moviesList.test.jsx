/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import { MoviesList } from './moviesList.jsx';

describe('<MoviesList />', () => {
  let wrapper;

  const moviesListProps = {
    movies: [
      {
        id: 1,
        title: 'Film Title 1',
        tagline: 'Film Tagline 1',
        vote_average: 5,
        vote_count: 100,
        release_date: '2019-09-09',
        poster_path:
          'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
        overview: 'Film overview 1',
        budget: 100000,
        revenue: 1000000,
        genres: ['Genre 1', 'Genre 2'],
        runtime: 100,
      },
      {
        id: 2,
        title: 'Film Title 2',
        tagline: 'Film Tagline 2',
        vote_average: 10,
        vote_count: 200,
        release_date: '2018-09-09',
        poster_path:
          'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
        overview: 'Film overview 2',
        budget: 200000,
        revenue: 2000000,
        genres: ['Genre 1', 'Genre 3'],
        runtime: 200,
      },
    ],
  };

  beforeEach(() => {
    wrapper = shallow(<MoviesList {...moviesListProps} />);
  });

  it('renders MoviesList element as expected', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('renders "No Films Found" text when there are no movies to show', () => {
    const expectedText = 'No Films Found';
    const newProps = { ...moviesListProps };
    newProps.movies = [];

    wrapper = shallow(<MoviesList {...newProps} />);

    const actualText = wrapper.find('.emptyResult').text();

    expect(actualText).toEqual(expectedText);
  });
});
