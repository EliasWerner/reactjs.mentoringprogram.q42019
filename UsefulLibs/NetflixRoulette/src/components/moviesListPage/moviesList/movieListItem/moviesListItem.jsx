import * as React from 'react';
import { Link } from 'react-router-dom';
import { movieListItemStyles } from './moviesListItemStyles';
import { Utils } from '../../../../utils/utils';
import type { MovieType } from '../../../../store/movies/moviesState';

type MovieListItemProps = {
  movie: MovieType,
};

export const MovieListItem = (props: MovieListItemProps) => {
  const styles = movieListItemStyles();

  return (
    <div className={styles.movieListItem}>
      <Link
        to={{
          pathname: `/film/${props.movie.id}`,
          state: {
            movieId: props.movie.id,
          },
        }}
        onClick={() => Utils.scrollTop()}
      >
        <img
          className={styles.moviePoster}
          src={props.movie.poster_path}
          alt={props.movie.title}
        />
      </Link>
      <div className={styles.movieInfo}>
        <div className={styles.movieTitle}>{props.movie.title}</div>
        <div className={styles.movieYear}>
          {new Date(props.movie.release_date).getFullYear()}
        </div>
      </div>
      <div className={styles.movieGenres}>{props.movie.genres.join(', ')}</div>
    </div>
  );
};
