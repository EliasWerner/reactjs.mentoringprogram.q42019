import { createUseStyles } from 'react-jss';

export const movieListItemStyles = createUseStyles({
  movieListItem: {
    color: '#dedede',
    fontFamily: 'Segoe UI Light',
    marginBottom: '50px',
  },

  moviePoster: {
    width: '100%',
    marginTop: '10px',
    marginBottom: '5px',
  },

  movieInfo: {
    display: 'inline-block',
    fontSize: '20px',
    marginTop: '5px',
    marginBottom: '10px',
  },

  movieTitle: {
    display: 'inline-block',
    width: '427px',
  },

  movieYear: {
    paddingTop: '2px',
    paddingBottom: '2px',
    paddingLeft: '10px',
    paddingRight: '10px',
    display: 'inline-block',
    borderRadius: '5px',
    borderColor: '#555555',
    borderStyle: 'solid',
    borderWidth: '1px',
    fontSize: '18px',
  },
});
