import * as React from 'react';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { MovieListItem } from './movieListItem/moviesListItem.jsx';
import { movieListStyles } from './moviesListStyles';
import type { MovieType } from '../../../store/movies/moviesState';

type MovieListProps = {
  movies: Array<MovieType>,
};

export const MoviesList = (props: MovieListProps) => {
  const styles = movieListStyles();

  return (
    <div className={styles.moviesList}>
      {props.movies && props.movies.length ? (
        <Container className={styles.moviesListContainer}>
          <Row>
            {props.movies.map((movie) => (
              <Col className={styles.movieColumn} key={movie.id} md={4}>
                <MovieListItem movie={movie} />
              </Col>
            ))}
          </Row>
        </Container>
      ) : (
        <div className={styles.emptyResult}>No Films Found</div>
      )}
    </div>
  );
};
