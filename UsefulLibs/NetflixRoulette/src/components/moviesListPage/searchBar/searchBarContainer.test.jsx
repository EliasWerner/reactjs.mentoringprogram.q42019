/* eslint-disable no-undef */
import { mapDispatchToProps, mapStateToProps } from './searchBarContainer';
import { initialMoviesState } from '../../../store/movies/moviesState';
import { SearchFilters } from '../../../store/movies/moviesActions';

describe('SearchBarContainer', () => {
  const initialState = {
    moviesState: initialMoviesState,
  };

  const ownProps = {
    searchBarText: 'some text',
    setSearchBarText: jest.fn(),
  };

  it('map state to props as expected', () => {
    const expectedResult = {
      searchBarText: ownProps.searchBarText,
      searchFilter: initialState.moviesState.searchFilter,
    };

    const actualResult = mapStateToProps(initialState, ownProps);

    expect(actualResult).toEqual(expectedResult);
  });

  it('map dispatch as expected', () => {
    const dispatch = jest.fn();

    mapDispatchToProps(dispatch, ownProps).setSearchFilter(
      SearchFilters.SEARCH_BY_GENRE
    );
    expect(dispatch).toHaveBeenCalled();

    mapDispatchToProps(dispatch, ownProps).searchMovies();
    expect(dispatch).toHaveBeenCalled();

    mapDispatchToProps(dispatch, ownProps).setSearchBarText();
    expect(ownProps.setSearchBarText).toHaveBeenCalled();
  });
});
