// @flow

import { connect } from 'react-redux';
import { SearchBar } from './searchBar.jsx';
import {
  setSearchFilter,
  searchMovies,
} from '../../../store/movies/moviesActions';
import type { MoviesStateType } from '../../../store/movies/moviesState';

export const mapStateToProps = (
  state: { moviesState: MoviesStateType },
  ownProps: { searchBarText: string }
) => ({
  searchBarText: ownProps.searchBarText,
  searchFilter: state.moviesState.searchFilter,
});

export const mapDispatchToProps = (
  dispatch: Function,
  ownProps: { setSearchBarText: Function }
) => ({
  setSearchFilter: (filter: string) => {
    dispatch(setSearchFilter(filter));
  },
  searchMovies: (filterText: string) => {
    dispatch(searchMovies(filterText));
  },
  setSearchBarText: ownProps.setSearchBarText,
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
