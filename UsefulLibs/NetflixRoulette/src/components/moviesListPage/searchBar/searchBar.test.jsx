/* eslint-disable no-undef */
import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { SearchBar } from './searchBar.jsx';
import { SearchFilters } from '../../../store/movies/moviesActions';

describe('<SearchBar />', () => {
  let wrapper;

  const searchBarProps = {
    isSearchByTitle: true,
    setSearchBarText: jest.fn(),
    searchMovies: jest.fn(),
    searchFilter: SearchFilters.SEARCH_BY_TITLE,
    setSearchFilter: jest.fn(),
    searchBarText: '',
  };

  beforeEach(() => {
    wrapper = shallow(<SearchBar {...searchBarProps} />);
  });

  it('renders SearchBar element as expected', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('setSearchBarText function is called on text field value changing', () => {
    wrapper = mount(<SearchBar {...searchBarProps} />);

    const searchField = wrapper.find('input');
    searchField.simulate('change');

    expect(searchBarProps.setSearchBarText).toHaveBeenCalled();
  });

  it('searchMovies function is called on "SEARCH" button click', () => {
    wrapper = mount(<SearchBar {...searchBarProps} />);

    const searchField = wrapper.find('.searchButton');
    searchField.simulate('click');

    expect(searchBarProps.searchMovies).toHaveBeenCalled();
  });

  it('setSearchFilter function is called on "TITLE" click', () => {
    const title = wrapper.find('.left');
    title.simulate('click');

    expect(searchBarProps.setSearchFilter).toHaveBeenCalledWith(
      SearchFilters.SEARCH_BY_TITLE
    );
  });

  it('setSearchFilter function is called on "GENRE" click', () => {
    const title = wrapper.find('.right');
    title.simulate('click');

    expect(searchBarProps.setSearchFilter).toHaveBeenCalledWith(
      SearchFilters.SEARCH_BY_GENRE
    );
  });

  it('"TITLE" does not have styles for selected element if searchFilter is equal to SEARCH_BY_GENRE', () => {
    const newProps = { ...searchBarProps };
    newProps.searchFilter = SearchFilters.SEARCH_BY_GENRE;
    wrapper = shallow(<SearchBar {...newProps} />);

    const title = wrapper.find('.left');

    expect(title.hasClass('selected')).toBeFalsy();
  });

  it('"GENRE" has styles for selected element if searchFilter is equal to SEARCH_BY_GENRE', () => {
    const newProps = { ...searchBarProps };
    newProps.searchFilter = SearchFilters.SEARCH_BY_GENRE;
    wrapper = shallow(<SearchBar {...newProps} />);

    const title = wrapper.find('.right');

    expect(title.hasClass('selected')).toBeTruthy();
  });
});
