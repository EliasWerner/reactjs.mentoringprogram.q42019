import { createUseStyles } from 'react-jss';

export const searchBarStyles = createUseStyles({
  searchBarContent: {
    color: '#ffffff',
    marginLeft: '150px',
  },

  searchBarTitle: {
    fontSize: '28px',
    marginTop: '10px',
    marginBottom: '10px',
    letterSpacing: '5px',
  },

  searchField: {
    fontSize: '24px',
    background: 'rgba(255, 255, 255, 0.2)',
    paddingTop: '5px',
    paddingBottom: '5px',
    paddingLeft: '10px',
    bottom: '0',
    color: '#ffffff',
    marginRight: '10px',

    width: '78%',
    marginTop: '10px',
    marginBottom: '10px',
    borderRadius: '5px',
    border: 0,
    letterSpacing: '0.01em',
  },

  searchButton: {
    fontSize: '24px',
    backgroundColor: '#f65251',
    paddingTop: '5px',
    paddingBottom: '5px',
    paddingLeft: '30px',
    paddingRight: '30px',
    borderRadius: '5px',
    border: 0,
    color: '#ffffff',
    fontWeight: 300,
  },

  searchFilter: {
    marginTop: '10px',
    marginBottom: '10px',
    display: 'inline-block',
  },

  searchFilterTitle: {
    display: 'inline-block',
    marginRight: '30px',
  },

  filterButton: {
    background: 'rgba(255, 255, 255, 0.2)',
    paddingTop: '5px',
    paddingBottom: '5px',
    paddingLeft: '20px',
    paddingRight: '20px',
    display: 'inline-block',
    cursor: 'pointer',
  },

  left: {
    borderTopLeftRadius: '5px',
    borderBottomLeftRadius: '5px',
  },

  right: {
    borderBottomRightRadius: '5px',
    borderTopRightRadius: '5px',
  },

  selected: {
    backgroundColor: '#f65251',
  },
});
