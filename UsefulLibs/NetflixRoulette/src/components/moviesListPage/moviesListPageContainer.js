import { connect } from 'react-redux';
import { MoviesListPage } from './moviesListPage.jsx';
import {
  getMovies,
  restore,
  searchMovies,
  setSearchFilter,
} from '../../store/movies/moviesActions';
import type {
  MoviesStateType,
  MovieType,
} from '../../store/movies/moviesState';

export const mapStateToProps = (state: { moviesState: MoviesStateType }) => ({
  movies: state.moviesState.moviesToShow.toArray(),
  moviesIsLoaded: !!state.moviesState.movies.toArray().length,
  pageSeparatorText: state.moviesState.pageSeparatorText,
});

export const mapDispatchToProps = (dispatch: Function) => ({
  getMovies: () => {
    dispatch(getMovies());
  },
  resetAppState: () => {
    dispatch(restore());
  },
  searchMovies: (searchString: string) => {
    dispatch(searchMovies(searchString));
  },
  setSearchFilter: (filter: string) => {
    dispatch(setSearchFilter(filter));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviesListPage);
