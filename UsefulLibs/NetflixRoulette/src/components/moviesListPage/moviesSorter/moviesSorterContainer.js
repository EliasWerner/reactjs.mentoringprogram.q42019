// @flow

import { connect } from 'react-redux';
import { MoviesSorter } from './moviesSorter.jsx';
import {
  setSortingFilter,
  sortMovies,
} from '../../../store/movies/moviesActions';
import type { MoviesStateType } from '../../../store/movies/moviesState';

export const mapStateToProps = (
  state: { moviesState: MoviesStateType },
  ownProps: { text: string }
) => ({
  text: ownProps.text,
  sortingFilter: state.moviesState.sortingFilter,
});

export const mapDispatchToProps = (dispatch: Function) => ({
  setSortingFilter: (filter: string) => {
    dispatch(setSortingFilter(filter));
  },
  sortMovies: () => {
    dispatch(sortMovies());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MoviesSorter);
