import { createUseStyles } from 'react-jss';

export const moviesSorterStyles = createUseStyles({
  searchResult: {
    display: 'inline-block',
    marginLeft: '150px',
    width: '500px',
    fontWeight: 'bold',
  },

  sorterTitle: {
    marginLeft: '800px',
    display: 'inline-block',
    marginRight: '30px',
  },

  sortButton: {
    background: 'rgba(255, 255, 255, 0.2)',
    paddingTop: '5px',
    paddingBottom: '5px',
    paddingLeft: '20px',
    paddingRight: '20px',
    display: 'inline-block',
    cursor: 'pointer',
  },

  left: {
    borderTopLeftRadius: '5px',
    borderBottomLeftRadius: '5px',
  },

  right: {
    borderBottomRightRadius: '5px',
    borderTopRightRadius: '5px',
  },

  selected: {
    backgroundColor: '#f65251',
  },
});
