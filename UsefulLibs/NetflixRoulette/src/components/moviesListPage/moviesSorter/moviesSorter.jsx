import * as React from 'react';
import { moviesSorterStyles } from './moviesSorterStyles';
import { SortingFilters } from '../../../store/movies/moviesActions';

type MoviesSorterProps = {
  sortMovies: Function,
  setSortingFilter: Function,
  sortingFilter: string,
  text: string,
};

export const MoviesSorter = (props: MoviesSorterProps) => {
  const styles = moviesSorterStyles();

  React.useEffect(() => props.sortMovies(), [props.sortingFilter]);

  return (
    <div>
      <div className={styles.searchResult}>{props.text}</div>
      <div className={styles.sorterTitle}>SORT BY</div>
      <div
        className={[
          styles.sortButton,
          styles.left,
          props.sortingFilter === SortingFilters.SORT_BY_RELEASE_DATE
            ? styles.selected
            : null,
        ].join(' ')}
        onClick={() => {
          props.setSortingFilter(SortingFilters.SORT_BY_RELEASE_DATE);
        }}
      >
        RELEASE DATE
      </div>
      <div
        className={[
          styles.sortButton,
          styles.right,
          props.sortingFilter === SortingFilters.SORT_BY_RATING
            ? styles.selected
            : null,
        ].join(' ')}
        onClick={() => {
          props.setSortingFilter(SortingFilters.SORT_BY_RATING);
        }}
      >
        RATING
      </div>
    </div>
  );
};
