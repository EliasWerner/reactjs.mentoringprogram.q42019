// @flow

/* eslint-disable operator-linebreak */
import * as React from 'react';
import loadable from '@loadable/component';
import { MoviesList } from './moviesList/moviesList.jsx';
import {
  SearchFilters,
  fetchMovies,
  fetchMoviesAndPerformSearch,
} from '../../store/movies/moviesActions';
import styles from '../moviePage/moviePage.css';
import type { MovieType } from '../../store/movies/moviesState';

const MoviesSorter = loadable(() =>
  import('./moviesSorter/moviesSorterContainer')
);
const SearchBar = loadable(() => import('./searchBar/searchBarContainer'));

type MoviesListPageProps = {
  history: {
    replace: Function,
  },
  location: {
    state: {
      shouldResetState: string | null,
    } | null,
    search: string,
    pathname: string,
  },
  match: {
    params: {
      searchQuery: string,
    },
    path: string,
  },
  movies: Array<MovieType>,
  pageSeparatorText: string,
  getMovies: Function,
  resetAppState: Function,
  moviesIsLoaded: boolean,
  searchMovies: Function,
  setSearchFilter: Function,
};

type MoviesListPageState = {
  searchBarText: string,
};

const initialState: MoviesListPageState = {
  searchBarText: '',
};

export class MoviesListPage extends React.Component<
  MoviesListPageProps,
  MoviesListPageState
> {
  constructor(props: MoviesListPageProps) {
    super(props);

    this.state = initialState;
  }

  static fetching({ dispatch, path }: { dispatch: Function, path: string }) {
    console.log(path);
    if (path === '/') {
      return [dispatch(fetchMovies())];
      // eslint-disable-next-line no-else-return
    } else {
      const { filter, search } = MoviesListPage.processPath(path);
      return filter && search
        ? [dispatch(fetchMoviesAndPerformSearch(filter, search))]
        : [];
    }
  }

  componentDidMount() {
    console.log(this.props);
    if (this.props.moviesIsLoaded && !this.props.location.state) {
      this.props.resetAppState();
    } else if (this.props.moviesIsLoaded && this.props.location.state) {
      this.processSearchQuery();
    } else if (!this.props.moviesIsLoaded) {
      this.props.getMovies();
    }
  }

  componentDidUpdate(prevProps: MoviesListPageProps) {
    if (
      prevProps.location.pathname !== this.props.location.pathname ||
      (!prevProps.moviesIsLoaded && this.props.moviesIsLoaded)
    ) {
      this.processSearchQuery();
    }

    if (
      this.props.location.state &&
      this.props.location.state.shouldResetState
    ) {
      this.setState(initialState);
      this.props.resetAppState();
      this.props.history.replace({ state: { shouldResetState: false } });
    }
  }

  render() {
    return (
      <div>
        <div className={styles.pageHeader}>
          <SearchBar
            setSearchBarText={this.setSearchBarText}
            searchBarText={this.state.searchBarText}
            history={this.props.history}
          />
        </div>
        <div className={styles.pageSeparator}>
          <MoviesSorter text={this.props.pageSeparatorText} />
        </div>
        <MoviesList movies={this.props.movies} />
      </div>
    );
  }

  setSearchBarText = (newText: string) => {
    this.setState({ searchBarText: newText });
  };

  static processPath = (path: string) => {
    const filterRegx = path.match(/filter=(.*)&/);
    const filter = filterRegx ? filterRegx.pop() : null;

    const searchRegx = path.match(/&search=(.*)$/);
    const search = searchRegx ? searchRegx.pop().replace('%20', ' ') : null;

    return { filter, search };
  };

  processSearchQuery = () => {
    if (this.props.match.path !== '/search') {
      return;
    }

    const { filter, search } = MoviesListPage.processPath(
      this.props.location.pathname
    );

    if (search && filter) {
      this.setState({ searchBarText: search });
      this.props.setSearchFilter(
        filter === 'genre'
          ? SearchFilters.SEARCH_BY_GENRE
          : SearchFilters.SEARCH_BY_TITLE
      );

      this.props.searchMovies(search);
    }
  };
}
