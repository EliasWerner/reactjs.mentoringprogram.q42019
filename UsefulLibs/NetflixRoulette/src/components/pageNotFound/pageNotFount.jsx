import * as React from 'react';
import { createUseStyles } from 'react-jss';

const pageNotFoundStyles = createUseStyles({
  pageNotFound: {
    backgroundColor: '#232323',
    fontFamily: 'Segoe UI Light',
    color: '#dedede',

    paddingTop: '410px',
    paddingBottom: '410px',
    textAlign: 'center',
    fontSize: '36px',
  },
});

export const PageNotFund = () => {
  const styles = pageNotFoundStyles();
  return (
    <div className={styles.pageNotFound}>
      <h1>404 - Page Not Found!</h1>
    </div>
  );
};
