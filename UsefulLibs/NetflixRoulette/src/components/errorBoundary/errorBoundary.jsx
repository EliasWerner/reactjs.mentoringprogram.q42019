// @flow

import * as React from 'react';
import Alert from 'react-bootstrap/Alert';

type ErrorBoundaryProps = {
  children: React.Node,
};

type ErrorBoundaryState = {
  hasError: boolean,
  errorMessage: string,
};

class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props: { children: React.Node }) {
    super(props);
    this.state = { hasError: false, errorMessage: '' };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  componentDidCatch(error: Error) {
    console.log(error);
    this.setState({ errorMessage: error.message });
  }

  render(): React.Node {
    if (this.state.hasError) {
      return (
        <Alert variant="danger" dismissible={false}>
          <Alert.Heading>Oh! You got an error!</Alert.Heading>
          <p>Something went wrong!</p>
          <p>Error: {this.state.errorMessage}</p>
        </Alert>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
