/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import ErrorBoundary from './errorBoundary.jsx';

describe('<ErrorBoundary />', () => {
  let wrapper;
  let spy;

  const ChildWithError = () => <div>Hello</div>;

  beforeEach(() => {
    wrapper = shallow(
      <ErrorBoundary>
        <ChildWithError />
      </ErrorBoundary>
    );
  });

  afterEach(() => {
    spy = jest.restoreAllMocks();
  });

  it('render: renders ErrorBoundary element as expected', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('componentDidCatch function is called on error throw', () => {
    spy = jest.spyOn(ErrorBoundary.prototype, 'componentDidCatch');
    const error = new Error('test');

    wrapper.find(ChildWithError).simulateError(error);

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
