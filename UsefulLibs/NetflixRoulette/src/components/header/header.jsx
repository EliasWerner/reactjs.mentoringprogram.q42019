import * as React from 'react';
import { Link } from 'react-router-dom';
import { createUseStyles } from 'react-jss';

const headerStyles = createUseStyles({
  header: {
    textAlign: 'left',
    fontSize: '24px',
    color: '#f65261',
    top: '10px',
    left: '50px',
    position: 'absolute',
    zIndex: 10,
    textDecoration: 'none',
    '&:hover': {
      color: '#f65261',
      textDecoration: 'none',
    },
  },
  netflix: {
    fontFamily: 'Franklin Gothic Heavy',
    letterSpacing: '0.01em',
  },
});

export const Header = () => {
  const styles = headerStyles();

  return (
    <header className={styles.header}>
      <Link
        to={{ pathname: '/', state: { shouldResetState: true } }}
        className={styles.header}
      >
        <b className={styles.netflix}>netflix</b>roulette
      </Link>
    </header>
  );
};
