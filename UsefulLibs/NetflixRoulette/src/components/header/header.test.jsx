/* eslint-disable no-undef */
import * as React from 'react';
import { shallow } from 'enzyme';
import { Header } from './header.jsx';

describe('<Header />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Header />);
  });

  it('renders Header element as expected', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
