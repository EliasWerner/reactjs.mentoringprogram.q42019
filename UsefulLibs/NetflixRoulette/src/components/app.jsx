// @flow

/* eslint-disable class-methods-use-this */
import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import loadable from '@loadable/component';
import { Provider } from 'react-redux';
import { Header } from './header/header.jsx';
import { Footer } from './footer/footer.jsx';
import ErrorBoundary from './errorBoundary/errorBoundary.jsx';
import { configureStore } from '../store/store';
import { PageNotFund } from './pageNotFound/pageNotFount.jsx';

const MoviesListPage = loadable(() =>
  import('./moviesListPage/moviesListPageContainer')
);
const MoviePage = loadable(() => import('./moviePage/moviePageContainer'));

const globalStore = configureStore();

class App extends React.Component<{}, {}> {
  render() {
    return (
      <div id="app" className="app">
        <Provider store={globalStore}>
          <ErrorBoundary>
            <Router>
              <Header />
              <Switch>
                <Route path="/" exact component={MoviesListPage} />
                <Route path="/search" component={MoviesListPage} />
                <Route path="/film/:id" component={MoviePage} />
                <Route component={PageNotFund} />
              </Switch>
              <Footer />
            </Router>
          </ErrorBoundary>
        </Provider>
      </div>
    );
  }
}

export default App;
