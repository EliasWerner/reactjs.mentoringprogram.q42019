import { PageNotFund } from '../components/pageNotFound/pageNotFount.jsx';
import MoviesListPage from '../components/moviesListPage/moviesListPageContainer';
import MoviePage from '../components/moviePage/moviePageContainer';

export default [
  {
    component: MoviesListPage,
    path: '/',
    exact: true,
  },
  {
    component: MoviesListPage,
    path: '/search/:query',
  },
  {
    component: MoviePage,
    path: '/film/:id',
  },
  {
    component: PageNotFund,
  },
];
