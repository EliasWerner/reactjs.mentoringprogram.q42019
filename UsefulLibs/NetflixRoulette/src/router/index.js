import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import Routes from './Routes';
import ErrorBoundary from '../components/errorBoundary/errorBoundary.jsx';
import { Header } from '../components/header/header.jsx';
import { Footer } from '../components/footer/footer.jsx';

const Router = () => (
  <div id="app">
    <ErrorBoundary>
      <BrowserRouter>
        <Header />
        <div>{renderRoutes(Routes)}</div>
        <Footer />
      </BrowserRouter>
    </ErrorBoundary>
  </div>
);

export default Router;
