const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  target: 'node',
  entry: './server/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'server.js',
  },
  resolve: {
    extensions: ['.jsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules/, /\.test.(js|jsx)$/],
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        include: /src/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
            },
          },
        ],
      },
    ],
  },
  externals: [webpackNodeExternals()],
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'servercss/[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: false,
    }),
  ],
};
