/* eslint-disable no-unused-vars */
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import path from 'path';
import { ChunkExtractor } from '@loadable/server';
import Routes from '../src/router/Routes';

const statsFile = path.resolve('dist/loadable-stats.json');

export default (pathname, store, context) => {
  const extractor = new ChunkExtractor({ statsFile, outputPath: '/dist/' });
  const jsx = extractor.collectChunks(
    <Provider store={store}>
      <StaticRouter location={pathname} context={context}>
        <div>{renderRoutes(Routes)}</div>
      </StaticRouter>
    </Provider>
  );

  const content = renderToString(jsx);

  return `
  <!DOCTYPE html>
      <html lang="en">
        <head>
          <meta charset="UTF-8">
          <title>Title</title>
          <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous"
          />
          <base href="http://localhost:8000/dist/" />
          <link href="main.css" rel="stylesheet" type="text/css" />
          ${extractor.getLinkTags()}
          ${extractor.getStyleTags()}
        </head>
        <body>
          <div id="root">${content}</div>
          ${extractor.getScriptTags()}
          <script>
            window.INITIAL_STATE = ${JSON.stringify(store.getState())}
          </script>
          <script src="bundle.js"></script>
        </body>
      </html>
  `;
};
