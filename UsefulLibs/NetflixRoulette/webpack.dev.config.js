const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const LinkTypePlugin = require('html-webpack-link-type-plugin')
  .HtmlWebpackLinkTypePlugin;

module.exports = (env) => {
  // eslint-disable-next-line operator-linebreak
  const currentMode =
    env && env.NODE_ENV === 'development' ? env.NODE_ENV : 'production';

  return {
    mode: currentMode,
    entry: ['./src/index.js'],

    devtool: currentMode === 'development' ? 'eval-source-map' : 'source-map',

    performance: { maxEntrypointSize: 9000000, maxAssetSize: 900000 },

    output: {
      path: path.join(__dirname, '/build'),
      filename: 'index-bundle.js',
    },

    resolve: {
      extensions: ['.jsx', '.js'],
    },

    devServer: {
      contentBase: './build',
      hot: true,
      historyApiFallback: true,
    },

    optimization: {
      splitChunks: {
        chunks: 'all',
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10,
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true,
          },
        },
      },
      minimizer: [
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true,
        }),
      ],
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: [/node_modules/, /\.test.(js|jsx)$/],
          use: ['babel-loader'],
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                hmr: currentMode === 'development',
              },
            },
            {
              loader: 'css-loader',
              query: {
                modules: true,
                localIdentName: '[name]__[local]___[hash:base64:5]',
              },
            },
          ],
        },
        {
          test: /\.(png|jpg)$/,
          loader: 'url-loader',
        },
      ],
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: './src/index.html',
      }),
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css',
        ignoreOrder: false,
      }),
      new LinkTypePlugin({
        '*.css': 'text/css',
      }),
    ],

    watch: currentMode === 'development',
  };
};
