/* eslint-disable import/no-extraneous-dependencies */

import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { createUseStyles } from 'react-jss';
import { MovieTitle } from '../src/components/moviePage/movieInfo/movieInfoForStoryBook.jsx';
import type { MovieType } from '../src/store/movies/moviesState';

const headerStyles = createUseStyles({
  movieInfo: {
    fontFamily: 'Segoe UI Light',
    color: '#ffffff',
    backgroundColor: '#555555',
    paddingTop: '10px',
    paddingBottom: '10px',
  },
  movieInfoRow: {
    display: 'inline-block',
  },
  movieInfoColumn: {
    display: 'inline-block',
    marginLeft: '50px',
    verticalAlign: 'top',
  },
  movieTitle: {
    display: 'inline-block',
    marginBottom: '10px',
  },
  rating: {
    display: 'inline-block',
    fontSize: '36px',
    color: '#a1e66f',
    borderRadius: '100%',
    borderColor: '#ffffff',
    borderStyle: 'solid',
    borderWidth: '1px',
    paddingLeft: '15px',
    paddingRight: '15px',
    marginLeft: '10px',
  },
  genres: {
    fontSize: '19px',
    marginBottom: '10px',
  },
  releaseInfo: {
    fontSize: '16px',
    marginBottom: '10px',
  },
  redText: {
    fontSize: '26px',
    color: '#f65261',
  },
  overview: {
    width: '1200px',
    fontSize: '22px',
  },
  homeLink: {
    textAlign: 'left',
    fontSize: '45px',
    color: '#f65261',
    top: '0px',
    position: 'absolute',
    textDecoration: 'none',
    marginLeft: '95%',
    '&:hover': {
      color: '#f65261',
      textDecoration: 'none',
    },
  },
  partialTitle: {
    display: 'inline-block',
  },
});

const movie: MovieType = {
  id: 0,
  title: 'Movie 1',
  vote_average: 6,
  vote_count: 200,
  overview:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam finibus felis et ullamcorper volutpat.',
  budget: 1000000,
  genres: ['genre 1', 'genre 2'],
  runtime: 100,
  release_date: 2019,
};

export default {
  title: 'MovieInfo Element',
};

export const movieInfoCustom = () => {
  const styles = headerStyles();

  return (
    <div className={styles.movieInfo}>
      <div className={styles.movieInfoRow}>
        <div className={styles.movieInfoColumn}>
          <div className={styles.movieTitle}>
            <div className={styles.partialTitle}>
              <MovieTitle movie={movie} />
            </div>
            <div className={styles.rating}>{movie.vote_average}</div>
          </div>
          <div className={styles.genres}>{movie.genres.join(', ')}</div>
          <div className={styles.releaseInfo}>
            <span className={styles.redText}>{movie.release_date}</span> year{' '}
            <span className={styles.redText}>{movie.runtime}</span> min
          </div>
          <div className={styles.overview}>{movie.overview}</div>
        </div>
      </div>
    </div>
  );
};
