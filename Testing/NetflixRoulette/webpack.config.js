const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = env => {
    const currentMode =
        env && env.NODE_ENV === 'development' ? env.NODE_ENV : 'production'

    return {
        mode: currentMode,
        entry: ['./src/index.js'],

        devtool:
            currentMode === 'development' ? 'eval-source-map' : 'source-map',

        performance: { maxEntrypointSize: 9000000, maxAssetSize: 900000 },

        output: {
            path: path.join(__dirname, '/dist'),
            filename: 'index-bundle.js',
        },

        resolve: {
            extensions: ['.jsx', '.js'],
        },

        devServer: {
            contentBase: './dist',
            hot: true,
        },

        optimization: {
            splitChunks: {
                chunks: 'all',
                cacheGroups: {
                    vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10,
                    },
                    default: {
                        minChunks: 2,
                        priority: -20,
                        reuseExistingChunk: true,
                    },
                },
            },
            minimizer: [
                new TerserPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: true,
                }),
            ],
        },

        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: [/node_modules/, /\.test.(js|jsx)$/],
                    use: ['babel-loader'],
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                hmr: currentMode === 'development',
                            },
                        },
                        'css-loader',
                    ],
                },
                {
                    test: /\.(png|jpg)$/,
                    loader: 'url-loader',
                },
            ],
        },

        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html',
            }),
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
                ignoreOrder: false,
            }),
        ],

        watch: currentMode === 'development',
    }
}
