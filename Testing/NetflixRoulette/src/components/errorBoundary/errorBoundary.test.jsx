import * as React from 'react'
import ErrorBoundary from './errorBoundary'
import { shallow } from 'enzyme'

describe('<ErrorBoundary />', () => {
    let wrapper
    let spy

    const ChildWithError = () => {
        return <div>Hello</div>
    }

    beforeEach(() => {
        wrapper = shallow(
            <ErrorBoundary>
                <ChildWithError />
            </ErrorBoundary>
        )
    })

    afterEach(() => {
        spy = jest.restoreAllMocks()
    })

    it('render: renders ErrorBoundary element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('componentDidCatch function is called on error throw', () => {
        spy = jest.spyOn(ErrorBoundary.prototype, 'componentDidCatch')
        const error = new Error('test')

        wrapper.find(ChildWithError).simulateError(error)

        expect(spy).toHaveBeenCalledTimes(1)
    })
})
