import * as React from 'react'
import Alert from 'react-bootstrap/Alert'

class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props)
        this.state = { hasError: false, errorMessage: '' }
    }

    static getDerivedStateFromError(error) {
        return { hasError: true }
    }

    componentDidCatch(error, errorInfo) {
        console.log(error)
        this.setState({ errorMessage: error.message })
    }

    render() {
        if (this.state.hasError) {
            return (
                <Alert variant="danger" dismissible={false}>
                    <Alert.Heading>Oh! You got an error!</Alert.Heading>
                    <p>Something went wrong!</p>
                    <p>Error: {this.state.errorMessage}</p>
                </Alert>
            )
        }

        return this.props.children
    }
}

export default ErrorBoundary
