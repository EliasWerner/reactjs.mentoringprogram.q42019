import * as React from 'react'
import { Footer } from './footer'
import { shallow } from 'enzyme'

describe('<Footer />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<Footer />)
    })

    it('renders Footer element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })
})
