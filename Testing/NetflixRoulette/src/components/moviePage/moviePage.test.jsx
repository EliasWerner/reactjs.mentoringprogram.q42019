import * as React from 'react'
import { MoviePage } from './moviePage'
import { shallow } from 'enzyme'
import { MoviesService } from '../../services/moviesService'

describe('<MoviePage />', () => {
    let wrapper

    const moviePageProps = {
        location: {
            state: 1,
        },
    }

    const foundMovie = {
        id: 1,
        title: 'Film Title 1',
        tagline: 'Film Tagline 1',
        vote_average: 5,
        vote_count: 100,
        release_date: '2019-09-09',
        poster_path:
            'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
        overview: 'Film overview',
        budget: 100000,
        revenue: 1000000,
        genres: ['Genre 1', 'Genre 2'],
        runtime: 100,
    }

    const moviesByGenre = [
        {
            id: 1,
            title: 'Film Title 1',
            tagline: 'Film Tagline 1',
            vote_average: 5,
            vote_count: 100,
            release_date: '2019-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 1',
            budget: 100000,
            revenue: 1000000,
            genres: ['Genre 1', 'Genre 2'],
            runtime: 100,
        },
        {
            id: 2,
            title: 'Film Title 2',
            tagline: 'Film Tagline 2',
            vote_average: 10,
            vote_count: 200,
            release_date: '2018-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 2',
            budget: 200000,
            revenue: 2000000,
            genres: ['Genre 1', 'Genre 3'],
            runtime: 200,
        },
    ]

    it('renders MoviePage element as expected', () => {
        MoviesService.getMoviesById = jest.fn(() => foundMovie)
        MoviesService.findMoviesByGenre = jest.fn(() => moviesByGenre)

        wrapper = shallow(<MoviePage {...moviePageProps} />)

        expect(wrapper).toMatchSnapshot()
    })
})
