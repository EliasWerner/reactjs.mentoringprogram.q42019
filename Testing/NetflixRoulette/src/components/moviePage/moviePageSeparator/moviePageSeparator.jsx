import * as React from 'react'
import './moviePageSeparator.css'

export const MoviePageSeparator = props => {
    return (
        <div className="moviePageSeparator">
            Movies by the {props.genre} genre
        </div>
    )
}
