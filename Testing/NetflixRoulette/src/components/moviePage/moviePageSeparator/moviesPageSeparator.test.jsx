import * as React from 'react'
import { MoviePageSeparator } from './moviePageSeparator'
import { shallow } from 'enzyme'

describe('<MoviePageSeparator />', () => {
    let wrapper

    const moviePageSeparatorProps = {
        genre: 'Genre 1',
    }

    beforeEach(() => {
        wrapper = shallow(<MoviePageSeparator {...moviePageSeparatorProps} />)
    })

    it('renders MoviePageSeparator element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })
})
