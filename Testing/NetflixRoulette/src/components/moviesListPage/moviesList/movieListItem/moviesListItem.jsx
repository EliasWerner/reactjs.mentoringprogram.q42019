import * as React from 'react'
import './moviesListItem.css'
import { Link } from 'react-router-dom'

export const MovieListItem = props => {
    return (
        <div className="movieListItem">
            <Link
                to={{
                    pathname: `/movie/${props.movie.id}`,
                    state: {
                        movieId: props.movie.id,
                    },
                }}
            >
                <img
                    className="moviePoster"
                    src={props.movie.poster_path}
                    alt={props.movie.title}
                />
            </Link>
            <div className="movieInfo">
                <div className="movieTitle">{props.movie.title}</div>
                <div className="movieYear">
                    {new Date(props.movie.release_date).getFullYear()}
                </div>
            </div>
            <div className="movieGenres">{props.movie.genres.join(', ')}</div>
        </div>
    )
}
