import * as React from 'react'
import { MovieListItem } from './moviesListItem'
import { shallow } from 'enzyme'

describe('<MovieListItem />', () => {
    let wrapper

    const movieListItemProps = {
        movie: {
            id: 1,
            title: 'Film Title',
            tagline: 'Film Tagline',
            vote_average: 5,
            vote_count: 100,
            release_date: '2019-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview',
            budget: 100000,
            revenue: 1000000,
            genres: ['Genre 1', 'Genre 2'],
            runtime: 100,
        },
    }

    beforeEach(() => {
        wrapper = shallow(<MovieListItem {...movieListItemProps} />)
    })

    it('renders MovieListItem element as expected', () => {
        expect(wrapper.exists()).toBeTruthy()
    })

    it('renders movie genres as expected', () => {
        const expectedMovieGenres = movieListItemProps.movie.genres.join(', ')

        const actualMovieGenres = wrapper.find('.movieGenres').text()

        expect(actualMovieGenres).toEqual(expectedMovieGenres)
    })
})
