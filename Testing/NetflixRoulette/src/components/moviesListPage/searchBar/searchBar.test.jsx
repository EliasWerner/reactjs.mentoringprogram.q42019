import * as React from 'react'
import { SearchBar } from './searchBar'
import { shallow, mount } from 'enzyme'

describe('<SearchBar />', () => {
    let wrapper

    const searchBarProps = {
        isSearchByTitle: true,
        setSearchBarText: jest.fn(),
        setIsSearchByTitle: jest.fn(),
        searchMovies: jest.fn(),
    }

    beforeEach(() => {
        wrapper = shallow(<SearchBar {...searchBarProps} />)
    })

    it('renders SearchBar element as expected', () => {
        expect(wrapper.exists()).toBeTruthy()
    })

    it('setSearchBarText function is not called on text field value changing when inputField.current is null', () => {
        const searchField = wrapper.find('input')
        searchField.simulate('change')

        expect(searchBarProps.setSearchBarText).not.toHaveBeenCalled()
    })

    it('searchMovies function is not called on "SEARCH" button click when inputField.current is null', () => {
        const searchField = wrapper.find('.searchButton')
        searchField.simulate('click')

        expect(searchBarProps.searchMovies).not.toHaveBeenCalled()
    })

    it('setSearchBarText function is called on text field value changing', () => {
        wrapper = mount(<SearchBar {...searchBarProps} />)

        const searchField = wrapper.find('input')
        searchField.simulate('change')

        expect(searchBarProps.setSearchBarText).toHaveBeenCalled()
    })

    it('searchMovies function is called on "SEARCH" button click', () => {
        wrapper = mount(<SearchBar {...searchBarProps} />)

        const searchField = wrapper.find('.searchButton')
        searchField.simulate('click')

        expect(searchBarProps.searchMovies).toHaveBeenCalled()
    })

    it('setIsSearchByTitle function is called on "TITLE" click', () => {
        const title = wrapper.find('.left')
        title.simulate('click')

        expect(searchBarProps.setIsSearchByTitle).toHaveBeenCalledWith(true)
    })

    it('setIsSearchByTitle function is called on "GENRE" click', () => {
        const title = wrapper.find('.right')
        title.simulate('click')

        expect(searchBarProps.setIsSearchByTitle).toHaveBeenCalledWith(false)
    })

    it('"TITLE" does not have styles for selected element if isSearchByTitle is equal to false', () => {
        const newProps = { ...searchBarProps }
        newProps.isSearchByTitle = false
        wrapper = shallow(<SearchBar {...newProps} />)

        const title = wrapper.find('.left')

        expect(title.hasClass('selected')).toBeFalsy()
    })

    it('"GENRE" has styles for selected element if isSearchByTitle is equal to false', () => {
        const newProps = { ...searchBarProps }
        newProps.isSearchByTitle = false
        wrapper = shallow(<SearchBar {...newProps} />)

        const title = wrapper.find('.right')

        expect(title.hasClass('selected')).toBeTruthy()
    })
})
