import * as React from 'react'
import { MoviesSorter } from './moviesSorter'
import { shallow } from 'enzyme'

describe('<MoviesSorter />', () => {
    let wrapper

    const moviesSorterProps = {
        text: 'Sorter text',
        isSortByReleaseDate: true,
        setIsSortByReleaseDate: jest.fn(),
    }

    beforeEach(() => {
        wrapper = shallow(<MoviesSorter {...moviesSorterProps} />)
    })

    it('renders MoviesSorter element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('setIsSortByReleaseDate function is called on "RELEASE DATE" click', () => {
        const releaseDate = wrapper.find('.left')
        releaseDate.simulate('click')

        expect(moviesSorterProps.setIsSortByReleaseDate).toHaveBeenCalledWith(
            true
        )
    })

    it('setIsSortByReleaseDate function is called on "RATING" click', () => {
        const releaseDate = wrapper.find('.right')
        releaseDate.simulate('click')

        expect(moviesSorterProps.setIsSortByReleaseDate).toHaveBeenCalledWith(
            false
        )
    })

    it('"RELEASE DATE" does not have styles for selected element if isSortByReleaseDate is equal to false', () => {
        const newProps = { ...moviesSorterProps }
        newProps.isSortByReleaseDate = false
        wrapper = shallow(<MoviesSorter {...newProps} />)

        const releaseDate = wrapper.find('.left')

        expect(releaseDate.hasClass('selected')).toBeFalsy()
    })
})
