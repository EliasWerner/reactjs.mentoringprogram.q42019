import * as React from 'react'
import { MoviesListPage } from './moviesListPage'
import { shallow } from 'enzyme'
import { MoviesService } from '../../services/moviesService'
import { Utils } from '../../utils/utils'

describe('<MoviesListPage />', () => {
    let wrapper
    let spy

    const initialState = {
        movies: [],
        filterResult: [],
        searchBarText: '',
        pageSeparatorText: '',
        isSearchByTitle: true,
        isSortByReleaseDate: true,
    }

    const props = {
        location: {
            state: '',
        },
    }

    const movies = [
        {
            id: 1,
            title: 'Film Title 1',
            tagline: 'Film Tagline 1',
            vote_average: 5,
            vote_count: 100,
            release_date: '2019-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 1',
            budget: 100000,
            revenue: 1000000,
            genres: ['Genre 1', 'Genre 2'],
            runtime: 100,
        },
        {
            id: 2,
            title: 'Film Title 2',
            tagline: 'Film Tagline 2',
            vote_average: 10,
            vote_count: 200,
            release_date: '2018-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 2',
            budget: 200000,
            revenue: 2000000,
            genres: ['Genre 1', 'Genre 3'],
            runtime: 200,
        },
    ]

    beforeEach(() => {
        wrapper = shallow(<MoviesListPage />)
    })

    afterEach(() => {
        spy = jest.restoreAllMocks()
    })

    it('renders MoviesListPage element', () => {
        expect(wrapper.exists()).toBeTruthy()
    })

    it('getMovies function is called inside componentDidMount function', () => {
        wrapper.instance().getMovies = jest.fn()

        wrapper.instance().componentDidMount()

        expect(wrapper.instance().getMovies).toHaveBeenCalled()
    })

    it('setState is not called inside componentWillReceiveProps function when location.state is empty string', () => {
        const newState = {
            ...initialState,
            searchBarText: 'movie title',
            movies,
        }
        wrapper.setState(newState)

        const newProps = { ...props }
        newProps.location.state = ''

        wrapper.instance().componentWillReceiveProps(newProps)

        const actualState = wrapper.state().searchBarText

        expect(actualState).toEqual(newState.searchBarText)
    })

    it('sets initial state if nextProps.location.state is equal to "resetState" ', () => {
        const newProps = { ...props }
        newProps.location.state = 'resetState'

        wrapper.setProps(newProps)

        const actualState = wrapper.state()

        expect(actualState).toEqual(initialState)
    })

    it('filterMovies function is called when prevState.movies is empty and state.movies is not empty', () => {
        wrapper.instance().filterMovies = jest.fn()
        wrapper.setState(initialState)

        wrapper.setState({ ...initialState, movies })

        expect(wrapper.instance().filterMovies).toHaveBeenCalled()
    })

    it('filterMovies function is called when prevState.isSortByReleaseDate is not equal to current isSortByReleaseDate', () => {
        wrapper.instance().filterMovies = jest.fn()
        wrapper.setState({ ...initialState, movies })

        wrapper.setState({
            ...initialState,
            movies,
            isSortByReleaseDate: !initialState.isSortByReleaseDate,
        })

        expect(wrapper.instance().filterMovies).toHaveBeenCalled()
    })

    it('MoviesService.getMovies function is called inside getMovies function', () => {
        MoviesService.getMoves = jest.fn(() => movies)

        wrapper.instance().getMovies()

        expect(MoviesService.getMoves).toHaveBeenCalled()
    })

    it('Utils.searchByTitle function is called if search field is not empty and isSearchByTitle is equal to true', () => {
        const newState = {
            ...initialState,
            searchBarText: 'movie title',
            movies,
        }
        wrapper.setState(newState)
        Utils.searchByTitle = jest.fn(() => movies)

        wrapper.instance().filterMovies()

        expect(Utils.searchByTitle).toHaveBeenCalledWith(
            newState.searchBarText,
            newState.movies
        )
    })

    it('Utils.searchByGenre function is called if search field is not empty and isSearchByTitle is equal to false', () => {
        const newState = {
            ...initialState,
            isSearchByTitle: false,
            searchBarText: 'movie genre',
            movies,
        }
        wrapper.setState(newState)
        Utils.searchByGenre = jest.fn(() => movies)

        wrapper.instance().filterMovies()

        expect(Utils.searchByGenre).toHaveBeenCalledWith(
            newState.searchBarText,
            newState.movies
        )
    })

    it('Utils.sortByReleaseDate function is called if isSortByReleaseDate is equal to true', () => {
        const newState = {
            ...initialState,
            isSortByReleaseDate: true,
            searchBarText: 'movie genre',
            movies,
        }
        wrapper.setState(newState)
        Utils.searchByGenre = jest.fn(() => movies)
        Utils.sortByReleaseDate = jest.fn(() => movies)

        wrapper.instance().filterMovies()

        expect(Utils.sortByReleaseDate).toHaveBeenCalled()
    })

    it('pageSeparatorText is equal to "2 movie found" when Utils.searchByTitle function returns array of two elements', () => {
        const newState = {
            ...initialState,
            searchBarText: 'movie title',
            movies,
        }
        wrapper.setState(newState)
        Utils.searchByTitle = jest.fn(() => movies)
        const expectedPageSeparatorText = '2 movie found'

        wrapper.instance().filterMovies()
        const actualPageSeparatorText = wrapper.state().pageSeparatorText

        expect(actualPageSeparatorText).toEqual(expectedPageSeparatorText)
    })

    it('pageSeparatorText is equal to empty string when Utils.searchByTitle function returns empty array', () => {
        const newState = {
            ...initialState,
            searchBarText: 'movie title',
            movies,
        }
        wrapper.setState(newState)
        Utils.searchByTitle = jest.fn(() => [])
        const expectedPageSeparatorText = ''

        wrapper.instance().filterMovies()
        const actualPageSeparatorText = wrapper.state().pageSeparatorText

        expect(actualPageSeparatorText).toEqual(expectedPageSeparatorText)
    })

    it('filterMovies function returns empty array when state.movies is null', () => {
        const newState = {
            ...initialState,
            movies: null,
        }
        wrapper.setState(newState)

        const expectedResult = []

        const actualResult = wrapper.instance().filterMovies()

        expect(actualResult).toEqual(expectedResult)
    })

    it('resetSearcResults function sets text to empty string', () => {
        const newState = {
            ...initialState,
            text: 'some text',
        }
        const expectedText = ''
        wrapper.setState(newState)

        wrapper.instance().resetSearcResults()
        const actualText = wrapper.state().text

        expect(actualText).toEqual(expectedText)
    })

    it('setSearchBarText function sets searchBarText as expected', () => {
        const expectedSearchBarText = 'Movie 1'

        wrapper.instance().setSearchBarText(expectedSearchBarText)
        const actualSearchBarText = wrapper.state().searchBarText

        expect(actualSearchBarText).toEqual(expectedSearchBarText)
    })

    it('setIsSearchByTitle function sets isSearchByTitle as expected', () => {
        const expectedIsSearchByTitle = false

        wrapper.instance().setIsSearchByTitle(expectedIsSearchByTitle)
        const actualIsSearchByTitle = wrapper.state().isSearchByTitle

        expect(actualIsSearchByTitle).toEqual(expectedIsSearchByTitle)
    })

    it('setIsSortByReleaseDate function sets isSortByReleaseDate as expected', () => {
        const expectedIsSortByReleaseDate = false

        wrapper.instance().setIsSortByReleaseDate(expectedIsSortByReleaseDate)
        const actualIsSortByReleaseDate = wrapper.state().isSortByReleaseDate

        expect(actualIsSortByReleaseDate).toEqual(expectedIsSortByReleaseDate)
    })
})
