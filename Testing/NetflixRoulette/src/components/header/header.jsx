import * as React from 'react'
import './header.css'
import { Link } from 'react-router-dom'

export const Header = () => (
    <header className="header">
        <Link to={{ pathname: '/', state: 'resetState' }} className="header">
            <b className="netflix">netflix</b>roulette
        </Link>
    </header>
)
