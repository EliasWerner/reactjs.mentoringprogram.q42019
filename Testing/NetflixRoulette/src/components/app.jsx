import * as React from 'react'
import { Header } from './header/header'
import { Footer } from './footer/footer'
import ErrorBoundary from './errorBoundary/errorBoundary'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { MoviesListPage } from './moviesListPage/moviesListPage'
import { MoviePage } from './moviePage/moviePage'

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <ErrorBoundary>
                    <Router>
                        <Header />
                        <Switch>
                            <Route exact path="/" component={MoviesListPage} />
                            <Route path="/movie/:id" component={MoviePage} />
                        </Switch>
                        <Footer />
                    </Router>
                </ErrorBoundary>
            </div>
        )
    }
}

export default App
