import * as moviesData from '../assets/data.json'

export class MoviesService {
    static movies = moviesData.data

    static getMoves = () => {
        return this.movies
    }

    static findMoviesByGenre = movieGenre => {
        const searchResult = this.movies.filter(movie =>
            movie.genres.some(
                genre => genre.toLowerCase() === movieGenre.toLowerCase()
            )
        )

        return searchResult
    }

    static findMoviesByTitle = movieTitle => {
        const searchResult = this.movies.filter(
            movie =>
                movie.title.toLowerCase().indexOf(movieTitle.toLowerCase()) !==
                -1
        )

        return searchResult
    }

    static getMoviesById = id => {
        const result = this.movies.filter(movie => movie.id === id)[0]

        return result
    }
}
