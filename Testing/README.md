# Task 4 (Testing lecture)

Coverage > 60%
Write tests using enzyme and jest
Use snapshot testing
Use coverage tool
Write at least one e2e test using library from the list:
* Cypress
* CasperJS
* Protractor
* Nightwatch
* Webdriver

## Evaluation criteria
2. Coverage > 60%.
3. Use snapshot testing.
4. Coverage > 80%. Functional testing with enzyme and jest.
5. Write at least one e2e test.