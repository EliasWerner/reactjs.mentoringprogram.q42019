import { MoviesService } from '../../services/moviesService'
import * as ActionTypes from './moviesActionTypes'
import { Utils } from '../../utils/utils'

/* SET_PAGE_SEPARATOR_TEXT */

export const setPageSeparatorText = text => ({
    text,
    type: ActionTypes.SET_PAGE_SEPARATOR_TEXT,
})

/* SET_MOVIES_WITH_THE_SAME_GENRE */

export const setMoviesWithTheSameGenre = movies => ({
    movies,
    type: ActionTypes.SET_MOVIES_WITH_THE_SAME_GENRE,
})

/* RESET_STORE */

export const setStore = store => ({
    store,
    type: ActionTypes.RESET_STORE,
})

/* SET_MOVIES */

export const setMovies = movies => ({
    movies,
    type: ActionTypes.SET_MOVIES,
})

export const setMoviesToShow = movies => ({
    movies,
    type: ActionTypes.SET_MOVIES_TO_SHOW,
})

/* SET_CURRENT_MOVIE */

export const setCurrentMovie = movie => ({
    movie,
    type: ActionTypes.SET_CURRENT_MOVIE,
})

/* SET_SEARCH_FILTER */
export const setSearchFilter = searchFilter => ({
    searchFilter,
    type: ActionTypes.SET_SEARCH_FILTER,
})

/* SET_SORTING_FILTER */
export const setSortingFilter = sortingFilter => ({
    sortingFilter,
    type: ActionTypes.SET_SORTING_FILTER,
})

export const getMovies = () => {
    return async dispatch => {
        try {
            const service = new MoviesService()
            const movies = await service.getMovies()

            const sortedMovies = Utils.sortByReleaseDate(movies)
            console.log(sortedMovies)
            dispatch(setMovies(sortedMovies))
        } catch (error) {
            throw new Error(`Could not get movies: ${error.message}`)
        }
    }
}

export const restore = () => {
    return async (dispatch, getState) => {
        const movies = getState().moviesState.movies

        const store = {
            movies,
            moviesToShow: [],
            moviesWithTheSameGenre: [],
            currentMovie: null,
            sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
            searchFilter: SearchFilters.SEARCH_BY_TITLE,
            pageSeparatorText: '',
        }

        console.log(store)
        dispatch(setStore(store))
    }
}

export const getMovieById = movieId => {
    return async dispatch => {
        try {
            const service = new MoviesService()
            const movie = await service.getMovieById(movieId)

            console.log(movie)
            dispatch(setCurrentMovie(movie))
        } catch (error) {
            throw new Error(`Could not get movie by id: ${error.message}`)
        }
    }
}

export const getMoviesWithTheSameGenre = () => {
    return async (dispatch, getState) => {
        const movie = getState().moviesState.currentMovie
        const movies = getState().moviesState.movies

        const moviesWithTheSameGenre = Utils.searchByGenre(
            movie.genres[0],
            movies
        )
        const filteredMovies = moviesWithTheSameGenre.filter(
            m => m.id !== movie.id
        )
        console.log(filteredMovies)

        dispatch(setMoviesWithTheSameGenre(filteredMovies))
    }
}

export const searchMovies = filterString => {
    return async (dispatch, getState) => {
        const searchFilter = getState().moviesState.searchFilter
        const movies = getState().moviesState.movies

        let filteredMovies = []
        switch (searchFilter) {
            case SearchFilters.SEARCH_BY_TITLE:
                filteredMovies = Utils.searchByTitle(filterString, movies)
                break
            case SearchFilters.SEARCH_BY_GENRE:
                filteredMovies = Utils.searchByGenre(filterString, movies)
                break
            default:
                break
        }

        const separatorText = filteredMovies.length
            ? `${filteredMovies.length} movie found`
            : ''

        console.log(filteredMovies)
        dispatch(setMoviesToShow(filteredMovies))
        dispatch(setPageSeparatorText(separatorText))
    }
}

export const sortMovies = () => {
    return async (dispatch, getState) => {
        const sortingFilter = getState().moviesState.sortingFilter
        const movies = getState().moviesState.moviesToShow

        let sortedMovies = []
        switch (sortingFilter) {
            case SortingFilters.SORT_BY_RELEASE_DATE: {
                sortedMovies = Utils.sortByReleaseDate(movies)
                break
            }
            case SortingFilters.SORT_BY_RATING: {
                sortedMovies = Utils.sortByRating(movies)
                break
            }
            default:
                break
        }

        console.log(sortedMovies)
        dispatch(setMoviesToShow(sortedMovies))
    }
}

export const SearchFilters = {
    SEARCH_BY_TITLE: 'SEARCH_BY_TITLE',
    SEARCH_BY_GENRE: 'SEARCH_BY_GENRE',
}

export const SortingFilters = {
    SORT_BY_RELEASE_DATE: 'SORT_BY_RELEASE_DATE',
    SORT_BY_RATING: 'SORT_BY_RATING',
}
