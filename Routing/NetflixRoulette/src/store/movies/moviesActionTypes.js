export const SET_MOVIES = 'SET_MOVIES'

export const SET_CURRENT_MOVIE = 'SET_CURRENT_MOVIE'

export const SET_SORTING_FILTER = 'SET_SORTING_FILTER'

export const SET_SEARCH_FILTER = 'SET_SEARCH_FILTER'

export const SET_MOVIES_TO_SHOW = 'SET_MOVIES_TO_SHOW'

export const SET_MOVIES_WITH_THE_SAME_GENRE = 'SET_MOVIES_WITH_THE_SAME_GENRE'

export const RESET_STORE = 'RESET_STORE'

export const SET_PAGE_SEPARATOR_TEXT = 'SET_PAGE_SEPARATOR_TEXT'
