import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import moviesReducer from './movies/moviesReducer'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const persistConfig = {
    key: 'root',
    storage,
}

const rootReducer = combineReducers({ moviesState: moviesReducer })
const persistedReducer = persistReducer(persistConfig, rootReducer)

export function configureStore() {
    const globalStore = createStore(
        persistedReducer,
        applyMiddleware(thunkMiddleware)
    )
    const persistor = persistStore(globalStore)
    return { globalStore, persistor }
}
