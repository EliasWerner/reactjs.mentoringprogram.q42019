import * as React from 'react'
import './pageNotFound.css'

export const PageNotFund = () => (
    <div className={'pageNotFound'}>
        <h1>404 - Page Not Found!</h1>
    </div>
)
