import { connect } from 'react-redux'
import { MoviePage } from './moviePage'
import {
    getMovieById,
    getMoviesWithTheSameGenre,
} from '../../store/movies/moviesActions'

export const mapStateToProps = state => {
    return {
        movie: state.moviesState.currentMovie,
        moviesToShow: state.moviesState.moviesWithTheSameGenre,
    }
}

export const mapDispatchToProps = dispatch => {
    return {
        getMovieById: id => {
            dispatch(getMovieById(id))
        },
        getMoviesWithTheSameGenre: () => {
            dispatch(getMoviesWithTheSameGenre())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviePage)
