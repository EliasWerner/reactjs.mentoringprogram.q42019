import * as React from 'react'
import './moviePageSeparator.css'
import PropTypes from 'prop-types'

export const MoviePageSeparator = props => {
    return (
        <div className="moviePageSeparator">
            Movies by the {props.genre} genre
        </div>
    )
}

MoviePageSeparator.propTypes = {
    genre: PropTypes.string,
}
