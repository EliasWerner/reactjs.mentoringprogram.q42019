import * as React from 'react'
import SearchBar from './searchBar/searchBarContainer'
import MoviesSorter from './moviesSorter/moviesSorterContainer'
import { MoviesList } from './moviesList/moviesList'
import PropTypes from 'prop-types'
import { SearchFilters } from '../../store/movies/moviesActions'

const initialState = {
    searchBarText: '',
}

export class MoviesListPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = initialState
    }

    componentDidMount() {
        if (this.props.moviesIsLoaded && !this.props.location.search) {
            this.props.resetAppState()
        } else if (this.props.moviesIsLoaded && this.props.location.search) {
            this.processSearchQuery()
        } else if (!this.props.moviesIsLoaded) {
            this.props.getMovies()
        }
    }

    componentDidUpdate(prevProps) {
        console.log(this.props)
        if (
            prevProps.location.search !== this.props.location.search ||
            (!prevProps.moviesIsLoaded &&
                this.props.moviesIsLoaded &&
                this.props.location.search)
        ) {
            this.processSearchQuery()
        }

        if (
            this.props.location.state &&
            this.props.location.state.shouldResetState
        ) {
            this.setState(initialState)
            this.props.resetAppState()
            this.props.history.replace({ state: { shouldResetState: false } })
        }
    }

    render() {
        return (
            <div>
                <div className="pageHeader">
                    <SearchBar
                        setSearchBarText={this.setSearchBarText}
                        searchBarText={this.state.searchBarText}
                        history={this.props.history}
                    />
                </div>
                <div className="pageSeparator">
                    <MoviesSorter text={this.props.pageSeparatorText} />
                </div>
                <MoviesList movies={this.props.movies} />
            </div>
        )
    }

    setSearchBarText = newText => {
        this.setState({ searchBarText: newText })
    }

    processSearchQuery = () => {
        const params = new URLSearchParams(this.props.location.search)
        const searchQuery = params.get('search')
        const filter = params.get('filter')

        if (searchQuery && filter) {
            this.setState({ searchBarText: searchQuery })
            this.props.setSearchFilter(
                filter === 'genre'
                    ? SearchFilters.SEARCH_BY_GENRE
                    : SearchFilters.SEARCH_BY_TITLE
            )
            console.log(searchQuery)
            console.log(filter)
            this.props.searchMovies(searchQuery)
        }
    }
}

const movieValidation = () =>
    PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    })

MoviesListPage.propTypes = {
    history: PropTypes.shape({
        replace: PropTypes.func,
    }),
    location: PropTypes.shape({
        state: PropTypes.string,
        search: PropTypes.string,
    }),
    match: PropTypes.shape({
        params: PropTypes.shape({
            searchQuery: PropTypes.string,
        }),
    }),
    movies: PropTypes.arrayOf(movieValidation),
    pageSeparatorText: PropTypes.string,
    getMovies: PropTypes.func,
    resetAppState: PropTypes.func,
    moviesIsLoaded: PropTypes.bool,
    searchMovies: PropTypes.func,
    setSearchFilter: PropTypes.func,
}
