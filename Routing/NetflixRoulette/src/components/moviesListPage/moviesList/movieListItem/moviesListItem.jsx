import * as React from 'react'
import './moviesListItem.css'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Utils } from '../../../../utils/utils'

export const MovieListItem = props => {
    return (
        <div className="movieListItem">
            <Link
                to={{
                    pathname: `/film/${props.movie.id}`,
                    state: {
                        movieId: props.movie.id,
                    },
                }}
                onClick={() => Utils.scrollTop()}
            >
                <img
                    className="moviePoster"
                    src={props.movie.poster_path}
                    alt={props.movie.title}
                />
            </Link>
            <div className="movieInfo">
                <div className="movieTitle">{props.movie.title}</div>
                <div className="movieYear">
                    {new Date(props.movie.release_date).getFullYear()}
                </div>
            </div>
            <div className="movieGenres">{props.movie.genres.join(', ')}</div>
        </div>
    )
}

MovieListItem.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    }),
}
