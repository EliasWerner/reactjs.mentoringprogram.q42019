import * as React from 'react'
import { MovieListItem } from './movieListItem/moviesListItem'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import './moviesList.css'
import PropTypes from 'prop-types'

export const MoviesList = props => {
    return (
        <div className="moviesList">
            {props.movies && props.movies.length ? (
                <Container className="moviesListContainer">
                    <Row>
                        {props.movies.map(movie => (
                            <Col className="movieColumn" key={movie.id} md={4}>
                                <MovieListItem movie={movie} />
                            </Col>
                        ))}
                    </Row>
                </Container>
            ) : (
                <div className="emptyResult">No Films Found</div>
            )}
        </div>
    )
}

const movieValidation = () =>
    PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    })

MoviesList.propTypes = {
    movies: PropTypes.arrayOf(movieValidation),
}
