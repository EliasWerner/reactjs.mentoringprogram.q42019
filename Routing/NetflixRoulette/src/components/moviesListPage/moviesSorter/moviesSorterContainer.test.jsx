/* eslint-disable no-undef */
import { mapDispatchToProps, mapStateToProps } from './moviesSorterContainer'
import { initialMoviesState } from '../../../store/movies/moviesState'
import { SortingFilters } from '../../../store/movies/moviesActions'

describe('MoviesSorterContainer', () => {
    const initialState = {
        moviesState: initialMoviesState,
    }

    const ownProps = {
        text: 'some text',
    }

    it('map state to props as expected', () => {
        const expectedResult = {
            text: ownProps.text,
            sortingFilter: initialMoviesState.sortingFilter,
        }

        const actualResult = mapStateToProps(initialState, ownProps)

        expect(actualResult).toEqual(expectedResult)
    })

    it('map dispatch as expected', () => {
        const dispatch = jest.fn()

        mapDispatchToProps(dispatch).setSortingFilter(
            SortingFilters.SORT_BY_RATING
        )
        expect(dispatch).toHaveBeenCalled()

        mapDispatchToProps(dispatch).sortMovies()
        expect(dispatch).toHaveBeenCalled()
    })
})
