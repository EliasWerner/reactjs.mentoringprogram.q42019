import { connect } from 'react-redux'
import { MoviesSorter } from './moviesSorter'
import {
    setSortingFilter,
    sortMovies,
} from '../../../store/movies/moviesActions'

export const mapStateToProps = (state, ownProps) => {
    return {
        text: ownProps.text,
        sortingFilter: state.moviesState.sortingFilter,
    }
}

export const mapDispatchToProps = dispatch => {
    return {
        setSortingFilter: filter => {
            dispatch(setSortingFilter(filter))
        },
        sortMovies: () => {
            dispatch(sortMovies())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviesSorter)
