import * as React from 'react'
import './searchBar.css'
import { SearchFilters } from '../../../store/movies/moviesActions'
import PropTypes from 'prop-types'

export const SearchBar = props => {
    return (
        <div className="searchBarContent">
            <div className="searchBarTitle">FIND YOUR MOVIE</div>
            <div>
                <input
                    className="searchField"
                    type="text"
                    placeholder="Search..."
                    value={props.searchBarText}
                    onChange={e => {
                        props.setSearchBarText(e.target.value)
                    }}
                ></input>
                <button
                    className="searchButton"
                    onClick={() => {
                        //props.searchMovies(props.searchBarText)
                        //props.history.push(`/search/${props.searchBarText}`)
                        props.history.push(
                            `/search?filter=${
                                props.searchFilter ===
                                SearchFilters.SEARCH_BY_TITLE
                                    ? 'title'
                                    : 'genre'
                            }&search=${props.searchBarText}`
                        )
                    }}
                >
                    SEARCH
                </button>
            </div>
            <div className="searchFilter">
                <div className="searchFilterTitle ">SEARCH BY</div>
                <div
                    className={`filterButton left ${
                        props.searchFilter === SearchFilters.SEARCH_BY_TITLE
                            ? 'selected'
                            : ''
                    }`}
                    onClick={() =>
                        props.setSearchFilter(SearchFilters.SEARCH_BY_TITLE)
                    }
                >
                    TITLE
                </div>
                <div
                    className={`filterButton right ${
                        props.searchFilter === SearchFilters.SEARCH_BY_GENRE
                            ? 'selected'
                            : ''
                    }`}
                    onClick={() =>
                        props.setSearchFilter(SearchFilters.SEARCH_BY_GENRE)
                    }
                >
                    GENRE
                </div>
            </div>
        </div>
    )
}

SearchBar.propTypes = {
    searchFilter: PropTypes.string,
    setSearchFilter: PropTypes.func,
    searchMovies: PropTypes.func,
    setSearchBarText: PropTypes.func,
    searchBarText: PropTypes.string,
    history: PropTypes.shape({
        push: PropTypes.func,
    }),
}
