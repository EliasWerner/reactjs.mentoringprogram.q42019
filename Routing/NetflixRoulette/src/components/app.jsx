import * as React from 'react'
import { Header } from './header/header'
import { Footer } from './footer/footer'
import ErrorBoundary from './errorBoundary/errorBoundary'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import MoviesListPage from './moviesListPage/moviesListPageContainer'
import MoviePage from './moviePage/moviePageContainer'
import { configureStore } from '../store/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { PageNotFund } from './pageNotFound/pageNotFount'

const { globalStore, persistor } = configureStore()

class App extends React.Component {
    render() {
        return (
            <div id="app" className="app">
                <Provider store={globalStore}>
                    <PersistGate loading={null} persistor={persistor}>
                        <ErrorBoundary>
                            <Router>
                                <Header />
                                <Switch>
                                    <Route
                                        path="/"
                                        exact
                                        component={MoviesListPage}
                                    />
                                    <Route
                                        path="/search"
                                        component={MoviesListPage}
                                    />
                                    <Route
                                        path="/film/:id"
                                        component={MoviePage}
                                    />
                                    <Route component={PageNotFund} />
                                </Switch>
                                <Footer />
                            </Router>
                        </ErrorBoundary>
                    </PersistGate>
                </Provider>
            </div>
        )
    }
}

export default App
