# Task 2 (Webpack lecture)

Create package.json file and install React, Redux, React-Redux, React-Router, Jest. Install and configure webpack & babel to get build artifact by running npm command.
Set DEV and PROD build configuration. Use env variables, dev server, optimizations for PROD buildSet up testing. You should have test command in your package.json file, which will run your future tests. Don't use React boilerplate for this task.

## Evaluation criteria
2. Installed React, Redux, React-Redux, React-Router, Jest.
3. Configured webpack.
4. Configured babel. Configured tests scripts.
5. Have dev and prod build commands (use env variables).