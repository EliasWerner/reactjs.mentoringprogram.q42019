import * as React from 'react'
import { MoviePageSeparator } from './moviePageSeparator/moviePageSeparator'
import { MovieInfo } from './movieInfo/movieInfo'
import { MoviesList } from '../moviesListPage/moviesList/moviesList'
import './moviePage.css'
import PropTypes from 'prop-types'
import Spinner from 'react-bootstrap/Spinner'

export class MoviePage extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const { movieId } = this.props.location.state

        this.props.getMovieById(movieId)
    }

    componentDidUpdate(prevProps) {
        const currentMovieId = this.props.location.state.movieId
        const prevMovieId = prevProps.location.state.movieId

        if (currentMovieId !== prevMovieId) {
            this.props.getMovieById(currentMovieId)
        }

        if (
            (!prevProps.movie && this.props.movie) ||
            prevProps.movie !== this.props.movie
        ) {
            this.props.getMoviesWithTheSameGenre()
        }
    }

    render() {
        return (
            <div>
                <div className="pageHeader">
                    {this.props.movie ? (
                        <MovieInfo movie={this.props.movie} />
                    ) : (
                        <Spinner animation="border" role="status">
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                    )}
                </div>
                <div className="pageSeparator">
                    {this.props.movie && (
                        <MoviePageSeparator
                            genre={this.props.movie.genres[0]}
                        />
                    )}
                </div>
                {this.props.movie && (
                    <MoviesList movies={this.props.moviesToShow} />
                )}
            </div>
        )
    }
}

MoviePage.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    }),
    moviesToShow: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            tagline: PropTypes.string,
            vote_average: PropTypes.number,
            vote_count: PropTypes.number,
            release_date: PropTypes.string,
            poster_path: PropTypes.string,
            overview: PropTypes.string,
            budget: PropTypes.number,
            revenue: PropTypes.number,
            genres: PropTypes.arrayOf(PropTypes.string),
            runtime: PropTypes.number,
        })
    ),
    location: PropTypes.shape({
        state: PropTypes.shape({
            movieId: PropTypes.number,
        }),
    }),
    getMovieById: PropTypes.func,
    getMoviesWithTheSameGenre: PropTypes.func,
}
