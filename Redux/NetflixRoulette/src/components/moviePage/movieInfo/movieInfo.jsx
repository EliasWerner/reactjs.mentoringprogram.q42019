import * as React from 'react'
import { Link } from 'react-router-dom'
import './movieInfo.css'
import PropTypes from 'prop-types'

export const MovieInfo = props => {
    const { movie } = props

    return (
        <div className="movieInfo">
            <Link
                to={{ pathname: '/', state: { shouldResetState: true } }}
                className="homeLink"
            >
                &#x2315;
            </Link>
            <div className="movieInfoRow">
                <div className="movieInfoColumn">
                    <img
                        className="moviePicture"
                        src={movie.poster_path}
                        alt={movie.title}
                    />
                </div>
                <div className="movieInfoColumn">
                    <div className="movieTitle">
                        <div className="title">{movie.title}</div>
                        <div className="rating">{movie.vote_average}</div>
                    </div>
                    <div className="genres">{movie.genres.join(', ')}</div>
                    <div className="releaseInfo">
                        <span className="redText">
                            {new Date(movie.release_date).getFullYear()}
                        </span>{' '}
                        year <span className="redText">{movie.runtime}</span>{' '}
                        min
                    </div>
                    <div className="overview">{movie.overview}</div>
                </div>
            </div>
        </div>
    )
}

MovieInfo.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    }),
}
