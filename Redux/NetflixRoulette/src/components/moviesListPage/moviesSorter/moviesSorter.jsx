import * as React from 'react'
import './moviesSorter.css'
import PropTypes from 'prop-types'
import { SortingFilters } from '../../../store/movies/moviesActions'

export const MoviesSorter = props => {
    React.useEffect(() => props.sortMovies(), props.sortingFilter)

    return (
        <div>
            <div className="searchResult">{props.text}</div>
            <div className="sorterTitle">SORT BY</div>
            <div
                className={`sortButton left ${
                    props.sortingFilter === SortingFilters.SORT_BY_RELEASE_DATE
                        ? 'selected'
                        : ''
                }`}
                onClick={() => {
                    props.setSortingFilter(SortingFilters.SORT_BY_RELEASE_DATE)
                }}
            >
                RELEASE DATE
            </div>
            <div
                className={`sortButton right ${
                    props.sortingFilter === SortingFilters.SORT_BY_RATING
                        ? 'selected'
                        : ''
                }`}
                onClick={() => {
                    props.setSortingFilter(SortingFilters.SORT_BY_RATING)
                }}
            >
                RATING
            </div>
        </div>
    )
}

MoviesSorter.propTypes = {
    text: PropTypes.string,
    sortingFilter: PropTypes.string,
    sortMovies: PropTypes.func,
    setSortingFilter: PropTypes.func,
}
