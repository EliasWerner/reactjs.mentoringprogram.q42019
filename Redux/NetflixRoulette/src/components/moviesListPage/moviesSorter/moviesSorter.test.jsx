/* eslint-disable no-undef */
import * as React from 'react'
import { MoviesSorter } from './moviesSorter'
import { shallow } from 'enzyme'
import { SortingFilters } from '../../../store/movies/moviesActions'

describe('<MoviesSorter />', () => {
    let wrapper

    const moviesSorterProps = {
        text: 'Sorter text',
        sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
        sortMovies: jest.fn(),
        setSortingFilter: jest.fn(),
    }

    beforeEach(() => {
        wrapper = shallow(<MoviesSorter {...moviesSorterProps} />)
    })

    it('renders MoviesSorter element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })

    it('setSortingFilter function is called on "RELEASE DATE" click', () => {
        const releaseDate = wrapper.find('.left')
        releaseDate.simulate('click')

        expect(moviesSorterProps.setSortingFilter).toHaveBeenCalledWith(
            SortingFilters.SORT_BY_RELEASE_DATE
        )
    })

    it('setSortingFilter function is called on "RATING" click', () => {
        const releaseDate = wrapper.find('.right')
        releaseDate.simulate('click')

        expect(moviesSorterProps.setSortingFilter).toHaveBeenCalledWith(
            SortingFilters.SORT_BY_RATING
        )
    })

    it('"RELEASE DATE" does not have styles for selected element if sortingFilter is equal to SORT_BY_RATING', () => {
        const newProps = { ...moviesSorterProps }
        newProps.sortingFilter = SortingFilters.SORT_BY_RATING
        wrapper = shallow(<MoviesSorter {...newProps} />)

        const releaseDate = wrapper.find('.left')

        expect(releaseDate.hasClass('selected')).toBeFalsy()
    })
})
