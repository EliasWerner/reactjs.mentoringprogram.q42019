import * as React from 'react'
import { Header } from './header/header'
import { Footer } from './footer/footer'
import ErrorBoundary from './errorBoundary/errorBoundary'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import MoviesListPage from './moviesListPage/moviesListPageContainer'
import MoviePage from './moviePage/moviePageContainer'
import { configureStore } from '../store/store'
import { Provider } from 'react-redux'
import { getMovies } from '../store/movies/moviesActions'
import { PersistGate } from 'redux-persist/integration/react'

const { globalStore, persistor } = configureStore()

class App extends React.Component {
    render() {
        return (
            <div className="app">
                <Provider store={globalStore}>
                    <PersistGate loading={null} persistor={persistor}>
                        <ErrorBoundary>
                            <Router>
                                <Header />
                                <Switch>
                                    <Route
                                        exact
                                        path="/"
                                        component={MoviesListPage}
                                    />
                                    <Route
                                        path="/movie/:id"
                                        component={MoviePage}
                                    />
                                </Switch>
                                <Footer />
                            </Router>
                        </ErrorBoundary>
                    </PersistGate>
                </Provider>
            </div>
        )
    }
}

export default App
