/* eslint-disable no-undef */
import * as React from 'react'
import App from './app'
import { shallow } from 'enzyme'

describe('<App />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<App />)
    })

    it('renders App element', () => {
        expect(wrapper.exists()).toBeTruthy()
    })
})
