/* eslint-disable no-undef */
import * as React from 'react'
import { Header } from './header'
import { shallow } from 'enzyme'

describe('<Header />', () => {
    let wrapper

    beforeEach(() => {
        wrapper = shallow(<Header />)
    })

    it('renders Header element as expected', () => {
        expect(wrapper).toMatchSnapshot()
    })
})
