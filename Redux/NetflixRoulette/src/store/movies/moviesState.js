import { SearchFilters } from './moviesActions'
import { SortingFilters } from './moviesActions'

export const initialMoviesState = {
    movies: [],
    moviesToShow: [],
    currentMovie: null,
    sortingFilter: SortingFilters.SORT_BY_RELEASE_DATE,
    searchFilter: SearchFilters.SEARCH_BY_TITLE,
    pageSeparatorText: '',
}
