import * as moviesActionTypes from './moviesActionTypes'
import { initialMoviesState } from './moviesState'

export const moviesReducer = (state = initialMoviesState, action) => {
    switch (action.type) {
        case moviesActionTypes.SET_CURRENT_MOVIE: {
            return { ...state, currentMovie: action.movie }
        }
        case moviesActionTypes.SET_MOVIES: {
            return { ...state, movies: action.movies }
        }
        case moviesActionTypes.SET_MOVIES_TO_SHOW: {
            return { ...state, moviesToShow: action.movies }
        }
        case moviesActionTypes.SET_SEARCH_FILTER: {
            return {
                ...state,
                searchFilter: action.searchFilter,
            }
        }
        case moviesActionTypes.SET_SORTING_FILTER: {
            return {
                ...state,
                sortingFilter: action.sortingFilter,
            }
        }
        case moviesActionTypes.RESET_STORE: {
            return {
                ...action.store,
            }
        }
        case moviesActionTypes.SET_PAGE_SEPARATOR_TEXT: {
            return {
                ...state,
                pageSeparatorText: action.text,
            }
        }
        default:
            return state
    }
}
export default moviesReducer
