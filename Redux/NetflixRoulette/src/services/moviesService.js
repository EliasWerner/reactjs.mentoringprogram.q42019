export class MoviesService {
    _apiEndpoint = 'https://reactjs-cdp.herokuapp.com/movies'

    getMovies = async () => {
        try {
            const response = await fetch(this._apiEndpoint)
            const json = await response.json()

            const movies = json.data

            return movies
        } catch (error) {
            throw new Error(`Error occured while web api:${error.message}`)
        }
    }

    getMovieById = async id => {
        try {
            const response = await fetch(`${this._apiEndpoint}/${id}`)
            const json = await response.json()

            const movie = json

            return movie
        } catch (error) {
            throw new Error(`Error occured while web api:${error.message}`)
        }
    }
}
