module.exports = {
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            './src/__mocks__/fileMock.js',
        '\\.(css|less)$': 'identity-obj-proxy',
    },
    collectCoverage: true,
    collectCoverageFrom: [
        '**/src/**',
        '**/*.{js,jsx}',
        '!*.config.js',
        '!**/node_modules/**',
        '!**/dist/**',
        '!**/tests/**',
        '!**/cypress/**',
        '!**/src/assets/**',
        '!**/server.js',
        '!**/index.js',
        '!**/src/**/__snapshots__/**',
        '!**/src/**/__mocks__/**',
    ],
    testPathIgnorePatterns: ['/dist/', '/node_modules/', '/cypress/'],
    snapshotSerializers: ['enzyme-to-json/serializer'],
    setupFilesAfterEnv: ['./enzyme.js'],
    coverageDirectory: './tests/coverage',
    moduleFileExtensions: ['js', 'jsx'],
    restoreMocks: true,
    verbose: true,
}
