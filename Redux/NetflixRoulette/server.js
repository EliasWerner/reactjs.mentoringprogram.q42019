const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')

const config = require('./webpack.config.js')
const prodConfig = config('production')
const compiler = webpack(prodConfig)

const port = process.env.PORT || 5000

const app = express()

app.use(
    webpackDevMiddleware(compiler, {
        publicPath: prodConfig.output.publicPath,
    })
)

app.get('/test_connection', (req, res) => {
    res.send({ connection: `Express server is started` })
})

app.listen(port, () => console.log(`Listening on port ${port}`))
