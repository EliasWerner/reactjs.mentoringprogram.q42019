# Task 5 (Flux + Redux)

1. Go through API docs in swagger: <https://reactjs-cdp.herokuapp.com/api-docs>
2. API Endpoint: <https://reactjs-cdp.herokuapp.com/>
3. Make your components perform real AJAX requests.
4. Move data fetches to actions and pass data to your components with redux.
5. Cover actions and reducers with unit tests.
6. Add the ability to store your apps state offline and use it to start-up the app.
You can take a look at redux-persist library for further reference.

## Evaluation criteria
2. All data fetches moved to actions & received from store by components.
3. Filtering and sorting is done as redux actions.
4. Actions and reducers covered with unit tests (~60%+, can be amended by mentor).
5. Offline data storage & store restoration (coverage ~100%).