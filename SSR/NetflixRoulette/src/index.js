/*import React from 'react'
import { hydrate } from 'react-dom'
import Router from './router'
import { Provider } from 'react-redux'
import { configureStore } from './store/store'

const store = configureStore()

hydrate(
    <Provider store={store}>
        <Router />
    </Provider>,
    document.getElementById('root')
)*/

import React from 'react'
import { hydrate } from 'react-dom'
import App from './components/app'

//hydrate(<App />, document.getElementById('root'))

import { loadableReady } from '@loadable/component'
loadableReady(() => {
    hydrate(<App />, document.getElementById('root'))
})
