import axios from 'axios'

export class MoviesService {
    _apiEndpoint = 'https://reactjs-cdp.herokuapp.com/movies'

    getMovies = async () => {
        try {
            const res = await axios.get(this._apiEndpoint)

            return res.data.data
        } catch (error) {
            throw new Error(`Error occured while web api:${error.message}`)
        }
    }

    getMovieById = async id => {
        try {
            const res = await axios.get(`${this._apiEndpoint}/${id}`)

            console.log(res.data)
            return res.data
        } catch (error) {
            throw new Error(`Error occured while web api:${error.message}`)
        }
    }
}
