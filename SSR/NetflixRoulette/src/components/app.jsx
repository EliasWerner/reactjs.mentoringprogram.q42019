import * as React from 'react'
import { Header } from './header/header'
import { Footer } from './footer/footer'
import ErrorBoundary from './errorBoundary/errorBoundary'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { configureStore } from '../store/store'
import { Provider } from 'react-redux'
import { PageNotFund } from './pageNotFound/pageNotFount'
import loadable from '@loadable/component'

const MoviesListPage = loadable(() =>
    import('./moviesListPage/moviesListPageContainer')
)
const MoviePage = loadable(() => import('./moviePage/moviePageContainer'))

const globalStore = configureStore()

class App extends React.Component {
    render() {
        return (
            <div id="app" className="app">
                <Provider store={globalStore}>
                    <ErrorBoundary>
                        <Router>
                            <Header />
                            <Switch>
                                <Route
                                    path="/"
                                    exact
                                    component={MoviesListPage}
                                />
                                <Route
                                    path="/search"
                                    component={MoviesListPage}
                                />
                                <Route path="/film/:id" component={MoviePage} />
                                <Route component={PageNotFund} />
                            </Switch>
                            <Footer />
                        </Router>
                    </ErrorBoundary>
                </Provider>
            </div>
        )
    }
}

export default App
