import * as React from 'react'
import styles from './loading.css'

export const Loading = () => <div className={styles.loading}>Loading...</div>
