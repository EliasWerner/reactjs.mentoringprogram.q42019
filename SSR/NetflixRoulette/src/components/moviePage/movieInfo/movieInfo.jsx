import * as React from 'react'
import { Link } from 'react-router-dom'
import styles from './movieInfo.css'
import PropTypes from 'prop-types'
import { Utils } from '../../../utils/utils'

export const MovieInfo = props => {
    const { movie } = props

    return (
        <>
            {!Utils.isEmptyObject(props.movie) ? (
                <div className={styles.movieInfo}>
                    <Link
                        to={{
                            pathname: '/',
                            state: { shouldResetState: true },
                        }}
                        className={styles.homeLink}
                    >
                        &#x2315;
                    </Link>{' '}
                    <div className={styles.movieInfoRow}>
                        <div className={styles.movieInfoColumn}>
                            <img
                                className={styles.moviePicture}
                                src={movie.poster_path}
                                alt={movie.title}
                            />
                        </div>
                        <div className={styles.movieInfoColumn}>
                            <div className={styles.movieTitle}>
                                <div className={styles.title}>
                                    {movie.title}
                                </div>
                                <div className={styles.rating}>
                                    {movie.vote_average}
                                </div>
                            </div>
                            <div className={styles.genres}>
                                {movie.genres.join(', ')}
                            </div>
                            <div className={styles.releaseInfo}>
                                <span className={styles.redText}>
                                    {new Date(movie.release_date).getFullYear()}
                                </span>{' '}
                                year{' '}
                                <span className={styles.redText}>
                                    {movie.runtime}
                                </span>{' '}
                                min
                            </div>
                            <div className={styles.overview}>
                                {movie.overview}
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <div className={styles.movieNotFound}>
                    <h1>Movie is not found</h1>
                </div>
            )}
        </>
    )
}

MovieInfo.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    }),
}
