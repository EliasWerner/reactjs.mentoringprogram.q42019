/* eslint-disable no-undef */
import { mapDispatchToProps, mapStateToProps } from './moviePageContainer'
import { initialMoviesState } from '../../store/movies/moviesState'

describe('MoviePageContainer', () => {
    const initialState = {
        moviesState: initialMoviesState,
    }

    it('map state to props as expected', () => {
        const expectedResult = {
            movie: initialMoviesState.currentMovie,
            moviesToShow: initialMoviesState.moviesToShow,
        }

        const actualResult = mapStateToProps(initialState)

        expect(actualResult).toEqual(expectedResult)
    })

    it('map dispatch as expected', () => {
        const dispatch = jest.fn()

        mapDispatchToProps(dispatch).getMovieById()
        expect(dispatch).toHaveBeenCalled()

        mapDispatchToProps(dispatch).getMoviesWithTheSameGenre()
        expect(dispatch).toHaveBeenCalled()
    })
})
