/* eslint-disable no-undef */
import * as React from 'react'
import { MoviePage } from './moviePage'
import { shallow } from 'enzyme'
import Spinner from 'react-bootstrap/Spinner'

describe('<MoviePage />', () => {
    let wrapper

    const foundMovie = {
        id: 1,
        title: 'Film Title 1',
        tagline: 'Film Tagline 1',
        vote_average: 5,
        vote_count: 100,
        release_date: '2019-09-09',
        poster_path:
            'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
        overview: 'Film overview',
        budget: 100000,
        revenue: 1000000,
        genres: ['Genre 1', 'Genre 2'],
        runtime: 100,
    }

    const moviesByGenre = [
        {
            id: 1,
            title: 'Film Title 1',
            tagline: 'Film Tagline 1',
            vote_average: 5,
            vote_count: 100,
            release_date: '2019-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 1',
            budget: 100000,
            revenue: 1000000,
            genres: ['Genre 1', 'Genre 2'],
            runtime: 100,
        },
        {
            id: 2,
            title: 'Film Title 2',
            tagline: 'Film Tagline 2',
            vote_average: 10,
            vote_count: 200,
            release_date: '2018-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 2',
            budget: 200000,
            revenue: 2000000,
            genres: ['Genre 1', 'Genre 3'],
            runtime: 200,
        },
    ]

    const moviePageProps = {
        location: {
            state: { movieId: 1 },
        },
        getMovieById: jest.fn(),
        getMoviesWithTheSameGenre: jest.fn(),

        movie: foundMovie,
        moviesToShow: moviesByGenre,
    }

    it('renders MoviePage element as expected', () => {
        wrapper = shallow(<MoviePage {...moviePageProps} />)

        expect(wrapper).toMatchSnapshot()
    })

    it('renders spinner if movie is null', () => {
        const newProps = { ...moviePageProps }
        newProps.movie = null

        wrapper = shallow(<MoviePage {...newProps} />)

        const spinner = wrapper.find(Spinner)

        expect(spinner.exists()).toBeTruthy()
    })

    it('getMovieById function is called if currentMovieId !== prevMovieId', () => {
        const firstProps = { ...moviePageProps }
        firstProps.location = {
            state: {
                movieId: 15,
            },
        }

        wrapper = shallow(<MoviePage {...firstProps} />)

        const secondProps = { ...moviePageProps }
        secondProps.location = {
            state: {
                movieId: 20,
            },
        }

        wrapper.setProps(secondProps)

        expect(secondProps.getMovieById).toHaveBeenCalledWith(
            secondProps.location.state.movieId
        )
    })

    it('getMoviesWithTheSameGenre function is called when prevProps.movie is null', () => {
        const firstProps = { ...moviePageProps }
        firstProps.movie = null

        wrapper = shallow(<MoviePage {...firstProps} />)

        const secondProps = { ...moviePageProps }
        firstProps.movie = moviePageProps.movie

        wrapper.setProps(secondProps)

        expect(secondProps.getMoviesWithTheSameGenre).toHaveBeenCalled()
    })

    it('getMoviesWithTheSameGenre function is called when prevProps.movie !== this.props.movie', () => {
        const firstProps = { ...moviePageProps }
        firstProps.movie = {
            ...moviePageProps,
            id: 5,
            overview: 'overview for movie with id = 5',
            genres: ['genre 1'],
        }

        wrapper = shallow(<MoviePage {...firstProps} />)

        const secondProps = { ...moviePageProps }
        firstProps.movie = {
            ...moviePageProps,
            id: 15,
            overview: 'overview for movie with id = 15',
            genres: ['genre 2'],
        }

        wrapper.setProps(secondProps)

        expect(secondProps.getMoviesWithTheSameGenre).toHaveBeenCalled()
    })
})
