import * as React from 'react'
import { MoviePageSeparator } from './moviePageSeparator/moviePageSeparator'
import { MovieInfo } from './movieInfo/movieInfo'
import { MoviesList } from '../moviesListPage/moviesList/moviesList'
import styles from './moviePage.css'
import PropTypes from 'prop-types'
import Spinner from 'react-bootstrap/Spinner'
import { Utils } from '../../utils/utils'
import { fetchMovieById } from '../../store/movies/moviesActions'

export class MoviePage extends React.Component {
    constructor(props) {
        super(props)
    }

    static fetching({ dispatch, path }) {
        return [dispatch(fetchMovieById(path.substr(6)))]
    }

    componentDidMount() {
        const { id } = this.props.match.params

        this.props.getMovieById(id)
    }

    componentDidUpdate(prevProps) {
        const currentMovieId = this.props.match.params.id
        const prevMovieId = prevProps.match.params.id

        if (currentMovieId !== prevMovieId) {
            this.props.getMovieById(currentMovieId)
        }

        if (prevProps.isLoading && !this.props.isLoading) {
            this.props.getMoviesWithTheSameGenre()
        }
    }

    render() {
        return (
            <div>
                <div className={styles.pageHeader}>
                    {!this.props.isLoading && this.props.movie ? (
                        <MovieInfo movie={this.props.movie} />
                    ) : (
                        <Spinner
                            animation="border"
                            role="status"
                            variant="light"
                        >
                            <span className="sr-only">Loading...</span>
                        </Spinner>
                    )}
                </div>
                <div className={styles.pageSeparator}>
                    {this.props.movie &&
                        !Utils.isEmptyObject(this.props.movie) && (
                            <MoviePageSeparator
                                genre={this.props.movie.genres[0]}
                            />
                        )}
                </div>
                {this.props.movie && !Utils.isEmptyObject(this.props.movie) && (
                    <MoviesList movies={this.props.moviesToShow} />
                )}
            </div>
        )
    }
}

MoviePage.propTypes = {
    movie: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    }),
    moviesToShow: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            tagline: PropTypes.string,
            vote_average: PropTypes.number,
            vote_count: PropTypes.number,
            release_date: PropTypes.string,
            poster_path: PropTypes.string,
            overview: PropTypes.string,
            budget: PropTypes.number,
            revenue: PropTypes.number,
            genres: PropTypes.arrayOf(PropTypes.string),
            runtime: PropTypes.number,
        })
    ),
    location: PropTypes.shape({
        state: PropTypes.shape({
            movieId: PropTypes.number,
        }),
    }),
    match: PropTypes.shape({
        params: PropTypes.shape({
            id: PropTypes.string,
        }),
    }),
    isLoading: PropTypes.bool,
    getMovieById: PropTypes.func,
    getMoviesWithTheSameGenre: PropTypes.func,
}
