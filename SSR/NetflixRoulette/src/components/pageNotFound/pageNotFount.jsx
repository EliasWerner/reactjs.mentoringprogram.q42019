import * as React from 'react'
import styles from './pageNotFound.css'

export const PageNotFund = () => (
    <div className={styles.pageNotFound}>
        <h1>404 - Page Not Found!</h1>
    </div>
)
