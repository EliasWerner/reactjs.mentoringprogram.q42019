import * as React from 'react'
import styles from './header.css'
import { Link } from 'react-router-dom'

export const Header = () => (
    <header className={styles.header}>
        <Link
            to={{ pathname: '/', state: { shouldResetState: true } }}
            className={styles.header}
        >
            <b className={styles.netflix}>netflix</b>roulette
        </Link>
    </header>
)
