/* eslint-disable no-undef */
import * as React from 'react'
import { MoviesListPage } from './moviesListPage'
import { shallow } from 'enzyme'

describe('<MoviesListPage />', () => {
    let wrapper

    const movies = [
        {
            id: 1,
            title: 'Film Title 1',
            tagline: 'Film Tagline 1',
            vote_average: 5,
            vote_count: 100,
            release_date: '2019-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 1',
            budget: 100000,
            revenue: 1000000,
            genres: ['Genre 1', 'Genre 2'],
            runtime: 100,
        },
        {
            id: 2,
            title: 'Film Title 2',
            tagline: 'Film Tagline 2',
            vote_average: 10,
            vote_count: 200,
            release_date: '2018-09-09',
            poster_path:
                'https://image.tmdb.org/t/p/w500/3kcEGnYBHDeqmdYf8ZRbKdfmlUy.jpg',
            overview: 'Film overview 2',
            budget: 200000,
            revenue: 2000000,
            genres: ['Genre 1', 'Genre 3'],
            runtime: 200,
        },
    ]

    const props = {
        location: {
            state: null,
        },
        getMovies: jest.fn(),
        history: {
            location: {},
            replace: jest.fn(),
        },
        movies,
        resetAppState: jest.fn(),
        moviesIsLoaded: false,
    }

    beforeEach(() => {
        wrapper = shallow(<MoviesListPage {...props} />)
    })

    it('renders MoviesListPage element', () => {
        expect(wrapper.exists()).toBeTruthy()
    })

    it('getMovies function is called inside componentDidMount function', () => {
        wrapper.instance().componentDidMount()

        expect(props.getMovies).toHaveBeenCalled()
    })

    it('getMovies function is not called when movies is already loaded', () => {
        const newProps = { ...props }
        newProps.moviesIsLoaded = true
        newProps.getMovies = jest.fn()

        wrapper = shallow(<MoviesListPage {...newProps} />)

        expect(newProps.getMovies).not.toHaveBeenCalled()
    })

    it('setSearchBarText function sets searchBarText as expected', () => {
        const expectedSearchBarText = 'Movie 1'

        wrapper.instance().setSearchBarText(expectedSearchBarText)
        const actualSearchBarText = wrapper.state().searchBarText

        expect(actualSearchBarText).toEqual(expectedSearchBarText)
    })
})
