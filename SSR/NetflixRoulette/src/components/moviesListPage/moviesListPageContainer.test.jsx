/* eslint-disable no-undef */
import { mapDispatchToProps, mapStateToProps } from './moviesListPageContainer'
import { initialMoviesState } from '../../store/movies/moviesState'

describe('MoviesListPageContainer', () => {
    const initialState = {
        moviesState: initialMoviesState,
    }

    it('map state to props as expected when movies is not loaded', () => {
        const expectedResult = {
            movies: initialMoviesState.moviesToShow,
            moviesIsLoaded: false,
            pageSeparatorText: '',
        }

        const actualResult = mapStateToProps(initialState)

        expect(actualResult).toEqual(expectedResult)
    })

    it('map state to props as expected when movies is loaded', () => {
        const newInitState = { ...initialState }
        newInitState.moviesState.movies = ['movie 1', 'movie 2']

        const expectedResult = {
            movies: initialMoviesState.moviesToShow,
            moviesIsLoaded: true,
            pageSeparatorText: '',
        }

        const actualResult = mapStateToProps(initialState)

        expect(actualResult).toEqual(expectedResult)
    })

    it('map dispatch as expected', () => {
        const dispatch = jest.fn()

        mapDispatchToProps(dispatch).getMovies()
        expect(dispatch).toHaveBeenCalled()

        mapDispatchToProps(dispatch).resetAppState()
        expect(dispatch).toHaveBeenCalled()
    })
})
