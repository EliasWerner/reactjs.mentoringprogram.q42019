import * as React from 'react'
import styles from './searchBar.css'
import { SearchFilters } from '../../../store/movies/moviesActions'
import PropTypes from 'prop-types'

export const SearchBar = props => {
    return (
        <div className={styles.searchBarContent}>
            <div className={styles.searchBarTitle}>FIND YOUR MOVIE</div>
            <div>
                <input
                    className={styles.searchField}
                    type="text"
                    placeholder="Search..."
                    value={props.searchBarText}
                    onChange={e => {
                        props.setSearchBarText(e.target.value)
                    }}
                ></input>
                <button
                    className={styles.searchButton}
                    onClick={() => {
                        props.history.push(
                            `/search/filter=${
                                props.searchFilter ===
                                SearchFilters.SEARCH_BY_TITLE
                                    ? 'title'
                                    : 'genre'
                            }&search=${props.searchBarText}`
                        )
                    }}
                >
                    SEARCH
                </button>
            </div>
            <div className={styles.searchFilter}>
                <div className={styles.searchFilterTitle}>SEARCH BY</div>
                <div
                    className={[
                        styles.filterButton,
                        styles.left,
                        props.searchFilter === SearchFilters.SEARCH_BY_TITLE
                            ? styles.selected
                            : null,
                    ].join(' ')}
                    onClick={() =>
                        props.setSearchFilter(SearchFilters.SEARCH_BY_TITLE)
                    }
                >
                    TITLE
                </div>
                <div
                    className={[
                        styles.filterButton,
                        styles.right,
                        props.searchFilter === SearchFilters.SEARCH_BY_GENRE
                            ? styles.selected
                            : null,
                    ].join(' ')}
                    onClick={() =>
                        props.setSearchFilter(SearchFilters.SEARCH_BY_GENRE)
                    }
                >
                    GENRE
                </div>
            </div>
        </div>
    )
}

SearchBar.propTypes = {
    searchFilter: PropTypes.string,
    setSearchFilter: PropTypes.func,
    searchMovies: PropTypes.func,
    setSearchBarText: PropTypes.func,
    searchBarText: PropTypes.string,
    history: PropTypes.shape({
        push: PropTypes.func,
    }),
}
