import { connect } from 'react-redux'
import { SearchBar } from './searchBar'
import {
    setSearchFilter,
    searchMovies,
} from '../../../store/movies/moviesActions'

export const mapStateToProps = (state, ownProps) => {
    return {
        searchBarText: ownProps.searchBarText,
        searchFilter: state.moviesState.searchFilter,
    }
}

export const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setSearchFilter: filter => {
            dispatch(setSearchFilter(filter))
        },
        searchMovies: filterText => {
            dispatch(searchMovies(filterText))
        },
        setSearchBarText: ownProps.setSearchBarText,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)
