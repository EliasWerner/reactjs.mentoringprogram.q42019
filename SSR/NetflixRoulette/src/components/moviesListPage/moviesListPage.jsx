import * as React from 'react'
import { MoviesList } from './moviesList/moviesList'
import PropTypes from 'prop-types'
import {
    SearchFilters,
    fetchMoviesAndPerformSearch,
} from '../../store/movies/moviesActions'
import styles from '../moviePage/moviePage.css'
import { fetchMovies } from '../../store/movies/moviesActions'
import loadable from '@loadable/component'

const MoviesSorter = loadable(() =>
    import('./moviesSorter/moviesSorterContainer')
)
const SearchBar = loadable(() => import('./searchBar/searchBarContainer'))

const initialState = {
    searchBarText: '',
}

export class MoviesListPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = initialState
    }

    static fetching({ dispatch, path }) {
        console.log(path)
        if (path === '/') {
            return [dispatch(fetchMovies())]
        } else {
            const { filter, search } = MoviesListPage.processPath(path)
            console.log(filter)
            console.log(search)

            return [dispatch(fetchMoviesAndPerformSearch(filter, search))]
        }
    }

    componentDidMount() {
        console.log(this.props)
        if (this.props.moviesIsLoaded && !this.props.location.state) {
            this.props.resetAppState()
        } else if (this.props.moviesIsLoaded && this.props.location.state) {
            this.processSearchQuery()
        } else if (!this.props.moviesIsLoaded) {
            this.props.getMovies()
        }
    }

    componentDidUpdate(prevProps) {
        console.log(this.props)
        if (
            prevProps.location.pathname !== this.props.location.pathname ||
            (!prevProps.moviesIsLoaded && this.props.moviesIsLoaded)
        ) {
            this.processSearchQuery()
        }

        if (
            this.props.location.state &&
            this.props.location.state.shouldResetState
        ) {
            this.setState(initialState)
            this.props.resetAppState()
            this.props.history.replace({ state: { shouldResetState: false } })
        }
    }

    render() {
        return (
            <div>
                <div className={styles.pageHeader}>
                    <SearchBar
                        setSearchBarText={this.setSearchBarText}
                        searchBarText={this.state.searchBarText}
                        history={this.props.history}
                    />
                </div>
                <div className={styles.pageSeparator}>
                    <MoviesSorter text={this.props.pageSeparatorText} />
                </div>
                <MoviesList movies={this.props.movies} />
            </div>
        )
    }

    setSearchBarText = newText => {
        this.setState({ searchBarText: newText })
    }

    static processPath = path => {
        const filter = path.match(/filter=(.*)&/).pop()
        const search = path
            .match(/&search=(.*)$/)
            .pop()
            .replace('%20', ' ')

        return { filter, search }
    }

    processSearchQuery = () => {
        if (this.props.match.path !== '/search') {
            return
        }

        const { filter, search } = MoviesListPage.processPath(
            this.props.location.pathname
        )

        if (search && filter) {
            this.setState({ searchBarText: search })
            this.props.setSearchFilter(
                filter === 'genre'
                    ? SearchFilters.SEARCH_BY_GENRE
                    : SearchFilters.SEARCH_BY_TITLE
            )
            console.log(search)
            console.log(filter)
            this.props.searchMovies(search)
        }
    }
}

const movieValidation = () =>
    PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    })

MoviesListPage.propTypes = {
    history: PropTypes.shape({
        replace: PropTypes.func,
    }),
    location: PropTypes.shape({
        state: PropTypes.string,
        search: PropTypes.string,
        pathname: PropTypes.string,
    }),
    match: PropTypes.shape({
        params: PropTypes.shape({
            searchQuery: PropTypes.string,
        }),
        path: PropTypes.string,
    }),
    movies: PropTypes.arrayOf(movieValidation),
    pageSeparatorText: PropTypes.string,
    getMovies: PropTypes.func,
    resetAppState: PropTypes.func,
    moviesIsLoaded: PropTypes.bool,
    searchMovies: PropTypes.func,
    setSearchFilter: PropTypes.func,
}
