import { connect } from 'react-redux'
import { MoviesListPage } from './moviesListPage'
import {
    getMovies,
    restore,
    searchMovies,
    setSearchFilter,
} from '../../store/movies/moviesActions'

export const mapStateToProps = state => {
    return {
        movies: state.moviesState.moviesToShow,
        moviesIsLoaded: state.moviesState.movies.length ? true : false,
        pageSeparatorText: state.moviesState.pageSeparatorText,
    }
}

export const mapDispatchToProps = dispatch => {
    return {
        getMovies: () => {
            dispatch(getMovies())
        },
        resetAppState: () => {
            dispatch(restore())
        },
        searchMovies: searchString => {
            dispatch(searchMovies(searchString))
        },
        setSearchFilter: filter => {
            dispatch(setSearchFilter(filter))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MoviesListPage)
