import * as React from 'react'
import { MovieListItem } from './movieListItem/moviesListItem'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import styles from './moviesList.css'
import PropTypes from 'prop-types'

export const MoviesList = props => {
    return (
        <div className={styles.moviesList}>
            {props.movies && props.movies.length ? (
                <Container className={styles.moviesListContainer}>
                    <Row>
                        {props.movies.map(movie => (
                            <Col
                                className={styles.movieColumn}
                                key={movie.id}
                                md={4}
                            >
                                <MovieListItem movie={movie} />
                            </Col>
                        ))}
                    </Row>
                </Container>
            ) : (
                <div className={styles.emptyResult}>No Films Found</div>
            )}
        </div>
    )
}

const movieValidation = () =>
    PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        tagline: PropTypes.string,
        vote_average: PropTypes.number,
        vote_count: PropTypes.number,
        release_date: PropTypes.string,
        poster_path: PropTypes.string,
        overview: PropTypes.string,
        budget: PropTypes.number,
        revenue: PropTypes.number,
        genres: PropTypes.arrayOf(PropTypes.string),
        runtime: PropTypes.number,
    })

MoviesList.propTypes = {
    movies: PropTypes.arrayOf(movieValidation),
}
