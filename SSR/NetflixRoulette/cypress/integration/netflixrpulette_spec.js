describe('NetflixRoulette', () => {
    it('Should show search bar title', () => {
        cy.visit('http://localhost:8080/')
        cy.get('.searchBarTitle').should('have.text', 'FIND YOUR MOVIE')
    })

    it('Should show empty search result', () => {
        const text = 'some random text'

        cy.visit('http://localhost:8080/')
        cy.get('input')
            .type(text)
            .should('have.value', text)
        cy.get('.searchButton').click()

        cy.get('.moviesList').contains('No Films Found')
    })

    it('"TITLE" button should not contain "selected" style when "GENRE" is selected ', () => {
        cy.visit('http://localhost:8080/')
        cy.get('.filterButton')
            .last()
            .click()
        cy.get('.filterButton')
            .last()
            .should('have.class', 'selected')
    })

    it('Should redirect to movie page on movie poster click', () => {
        cy.visit('http://localhost:8080/')
        cy.get('.moviePoster')
            .first()
            .click()
        cy.location('pathname').should('eq', '/movie/333339')
    })

    it('Movie page should show text on "page separator"', () => {
        cy.visit('http://localhost:8080/')
        cy.get('.moviePoster')
            .first()
            .click()
        cy.get('.pageSeparator').should(
            'have.text',
            'Movies by the Adventure genre'
        )
    })
})
