export class Utils {
    static sortByReleaseDate = movies => {
        const sortedMovies = [...movies]

        sortedMovies.sort((a, b) =>
            new Date(a.release_date) === new Date(b.release_date)
                ? 0
                : new Date(a.release_date) < new Date(b.release_date)
                ? 1
                : -1
        )

        return sortedMovies
    }

    static sortByRating = movies => {
        const sortedMovies = [...movies]

        sortedMovies.sort((a, b) =>
            a.vote_average === b.vote_average
                ? 0
                : a.vote_average > b.vote_average
                ? -1
                : 1
        )

        return sortedMovies
    }

    static searchByGenre = (movieGenre, movies) => {
        const searchResult = movies.filter(movie =>
            movie.genres.some(
                genre => genre.toLowerCase() === movieGenre.toLowerCase()
            )
        )

        return searchResult
    }

    static searchByTitle = (movieTitle, movies) => {
        const searchResult = movies.filter(
            movie =>
                movie.title.toLowerCase().indexOf(movieTitle.toLowerCase()) !==
                -1
        )

        return searchResult
    }
}
