import * as React from 'react'
import { MoviePageSeparator } from './moviePageSeparator/moviePageSeparator'
import { MovieInfo } from './movieInfo/movieInfo'
import { MoviesService } from '../../services/moviesService'
import { MoviesList } from '../moviesListPage/moviesList/moviesList'
import './moviePage.css'

export const MoviePage = props => {
    const { movieId } = props.location.state
    const movie = MoviesService.getMoviesById(movieId)
    const movies = MoviesService.findMoviesByGenre(movie.genres[0])
    const movesWithTheSameGenre = movies.filter(m => m.id !== movie.id)

    return (
        <div>
            <div className="pageHeader">
                <MovieInfo movie={movie} />
            </div>
            <div className="pageSeparator">
                <MoviePageSeparator genre={movie.genres[0]} />
            </div>
            <MoviesList movies={movesWithTheSameGenre} />
        </div>
    )
}
