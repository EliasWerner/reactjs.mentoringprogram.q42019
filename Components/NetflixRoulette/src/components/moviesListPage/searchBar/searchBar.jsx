import * as React from 'react'
import './searchBar.css'

export const SearchBar = props => {
    const inputField = React.createRef()

    return (
        <div className="searchBarContent">
            <div className="searchBarTitle">FIND YOUR MOVIE</div>
            <div>
                <input
                    className="searchField"
                    type="text"
                    placeholder="Search..."
                    ref={inputField}
                    onChange={() => {
                        if (inputField.current) {
                            props.setSearchBarText(inputField.current.value)
                        }
                    }}
                ></input>
                <button
                    className="searchButton"
                    onClick={() => {
                        if (inputField.current) {
                            props.searchMovies()
                        }
                    }}
                >
                    SEARCH
                </button>
            </div>
            <div className="searchFilter">
                <div className="searchFilterTitle ">SEARCH BY</div>
                <div
                    className={`filterButton left ${
                        props.isSearchByTitle ? 'selected' : null
                    }`}
                    onClick={() => props.setIsSearchByTitle(true)}
                >
                    TITLE
                </div>
                <div
                    className={`filterButton right ${
                        !props.isSearchByTitle ? 'selected' : null
                    }`}
                    onClick={() => props.setIsSearchByTitle(false)}
                >
                    GENRE
                </div>
            </div>
        </div>
    )
}
