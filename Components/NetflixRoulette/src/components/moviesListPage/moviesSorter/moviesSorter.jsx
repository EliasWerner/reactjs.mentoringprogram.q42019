import * as React from 'react'
import './moviesSorter.css'

export const MoviesSorter = props => {
    return (
        <div>
            <div className="searchResult">{props.text}</div>
            <div className="sorterTitle">SORT BY</div>
            <div
                className={`sortButton left ${
                    props.isSortByReleaseDate ? 'selected' : null
                }`}
                onClick={() => {
                    props.setIsSortByReleaseDate(true)
                }}
            >
                RELEASE DATE
            </div>
            <div
                className={`sortButton right ${
                    !props.isSortByReleaseDate ? 'selected' : null
                }`}
                onClick={() => {
                    props.setIsSortByReleaseDate(false)
                }}
            >
                RATING
            </div>
        </div>
    )
}
