import * as React from 'react'
import { MovieListItem } from './movieListItem/moviesListItem'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import './moviesList.css'

export const MoviesList = props => {
    return (
        <div className="moviesList">
            {props.movies && props.movies.length ? (
                <Container className="moviesListContainer">
                    <Row>
                        {props.movies.map(movie => (
                            <Col className="movieColumn" key={movie.id} md={4}>
                                <MovieListItem movie={movie} />
                            </Col>
                        ))}
                    </Row>
                </Container>
            ) : (
                <div className="emptyResult">No Films Found</div>
            )}
        </div>
    )
}
