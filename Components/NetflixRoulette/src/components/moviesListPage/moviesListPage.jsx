import * as React from 'react'
import { SearchBar } from './searchBar/searchBar'
import { MoviesSorter } from './moviesSorter/moviesSorter'
import { MoviesList } from './moviesList/moviesList'
import { MoviesService } from '../../services/moviesService'
import { Utils } from '../../utils/utils'

const initialState = {
    movies: [],
    filterResult: [],
    searchBarText: '',
    pageSeparatorText: '',
    isSearchByTitle: true,
    isSortByReleaseDate: true,
}

export class MoviesListPage extends React.Component {
    constructor(props) {
        super(props)

        this.state = initialState
    }

    componentDidMount() {
        this.getMovies()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.state === 'resetState') {
            this.setState(initialState)
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.state.movies.length) {
            this.getMovies()
        }

        if (!prevState.movies.length && this.state.movies.length) {
            this.filterMovies()
        }

        if (prevState.isSortByReleaseDate !== this.state.isSortByReleaseDate) {
            this.filterMovies()
        }
    }

    render() {
        return (
            <div>
                <div className="pageHeader">
                    <SearchBar
                        setSearchBarText={this.setSearchBarText}
                        searchBarText={this.state.searchBarText}
                        isSearchByTitle={this.state.isSearchByTitle}
                        setIsSearchByTitle={this.setIsSearchByTitle}
                        searchMovies={this.filterMovies}
                    />
                </div>
                <div className="pageSeparator">
                    <MoviesSorter
                        isSortByReleaseDate={this.state.isSortByReleaseDate}
                        setIsSortByReleaseDate={this.setIsSortByReleaseDate}
                        text={this.state.pageSeparatorText}
                    />
                </div>
                <MoviesList movies={this.state.filterResult} />
            </div>
        )
    }

    getMovies = () => {
        const movies = MoviesService.getMoves()

        this.setState({ movies })
    }

    filterMovies = () => {
        if (this.state.movies && this.state.movies.length) {
            let filteredMovies = [...this.state.movies]
            if (this.state.searchBarText !== '') {
                filteredMovies = this.state.isSearchByTitle
                    ? Utils.searchByTitle(
                          this.state.searchBarText,
                          this.state.movies
                      )
                    : Utils.searchByGenre(
                          this.state.searchBarText,
                          this.state.movies
                      )

                this.setState({
                    pageSeparatorText: filteredMovies.length
                        ? `${filteredMovies.length} movie found`
                        : '',
                })
            }

            const sortedMovies = this.state.isSortByReleaseDate
                ? Utils.sortByReleaseDate(filteredMovies)
                : Utils.sortByRating(filteredMovies)

            this.setState({ filterResult: sortedMovies })
        }

        return []
    }

    resetSearcResults = () => {
        this.setState({ searchResult: null, text: '' })
    }

    setSearchBarText = newText => {
        this.setState({ searchBarText: newText })
    }

    setIsSearchByTitle = isSearchByTitle => {
        this.setState({ isSearchByTitle })
    }

    setIsSortByReleaseDate = isSortByReleaseDate => {
        this.setState({ isSortByReleaseDate })
    }
}
