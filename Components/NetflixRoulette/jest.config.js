module.exports = {
    collectCoverage: true,
    collectCoverageFrom: [
        '**/src/**',
        '**/*.{js,jsx}',
        '!*.config.js',
        '!**/node_modules/**',
        '!**/dist/**',
        '!**/tests/**',
        '!**/server.js',
    ],
    coverageDirectory: './tests/coverage',
    moduleFileExtensions: ['js', 'jsx'],
    restoreMocks: true,
    verbose: true,
}
