# Task 3 (Components)

Write components implementing HTML markup for required design (see images at the beginning of the document). For this part, no need to implement API calls and routing, the task can be done with mocked data.
Use <ErrorBoundary> component for catching and displaying errors
<https://reactjs.org/docs/error-boundaries.html>. You could create one component and wrap all your application, or use several components.

## Evaluation criteria
2. Markup is done with React Components.
3. Use <ErrorBoundary> component for catching and displaying errors.
4. Use smart/dumb components approach
5. 100% decomposition (evaluated by mentor)