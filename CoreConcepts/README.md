# Task 1 (Core concepts lecture)

## Setting up the environment
Install and launch "blank" node.js application. You'll need it in further development for implementing SSR. Commit the changes to repository.
***Don't use any generators (like create-react-app)***

### Create components in different ways, using:
* React.createElement
* React.Component
* React.PureComponent
* Functional components

## Evaluation criteria
2. Install blanck express.js application.
3. Render blank message (Hello World) with react.
4. Use at least 2 methods of creating react component.
5. Use all methods which mentioned in task, to create react components.